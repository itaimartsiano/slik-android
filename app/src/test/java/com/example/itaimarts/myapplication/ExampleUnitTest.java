package com.example.itaimarts.myapplication;


import com.example.itaimarts.myapplication.domain.model.Payment;
import com.example.itaimarts.myapplication.domain.model.Table;
import com.example.itaimarts.myapplication.domain.model.User;
import com.example.itaimarts.myapplication.network.checkoutServer.GetMe;
import com.example.itaimarts.myapplication.network.checkoutServer.GetPaymentStatus;
import com.example.itaimarts.myapplication.network.checkoutServer.GetTableInfo;
import com.example.itaimarts.myapplication.network.checkoutServer.InitPaymentProcess;
import com.example.itaimarts.myapplication.network.checkoutServer.Login;
import com.example.itaimarts.myapplication.network.checkoutServer.UpdateMe;

import org.junit.Ignore;
import org.junit.Test;

import java.util.Date;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

public class ExampleUnitTest {

    //TODO: all the test are not available - need to fix them allllllll
    @Ignore
    @Test
    public void checkGetMe(){
        GetMe getMe = new GetMe();
        User userRest = getMe.send("this_is_session_id");
        assertEquals ("user birthday is not equal",userRest.birthday,(new Date(92,00,21,00,00,00)));
    }
    @Ignore
    @Test
    public void checkUpdateMe(){
        UpdateMe updateMe = new UpdateMe();
        User userRest = updateMe.updateMe("this_is_session_id",null,"facebook.com");
        System.out.println(userRest.name);
        assertEquals ("user birthday is not equal",userRest.birthday,(new Date(92,00,21,00,00,00)));
    }
    @Ignore
    @Test
    public void checkGetTableDetails() throws Exception{
        GetTableInfo tableDetails = new GetTableInfo();
        String sessionID = "this_is_session_id";
        String groupId = "GROUPIDENT";
        Table groupInfoDetailsRest = tableDetails.getTableDetails(sessionID,groupId);
        assertEquals(groupInfoDetailsRest.bill.products.get(0), "Roy Sommer");
    }

    @Ignore
    @Test
    public void checkLogin() throws Exception{
        Login login = new Login();
        String faceboookAccessToken = "EAADYY61xCQgBAEHDPrjIcq11WzexKHsp65PazKPsBg4hR6M55Tn4UlPchCmKcm8bXgidSHZCW6VRMTfALBXA6fmMJWZBKL4MZASsZC9YzWZC81jw6AojKWcZAXNpvjXEJSHnM0MMsXLdo7207tWezbNFNCHTnrgQTLkQGa9qUhqAZDZD";
        User userRest = login.send(faceboookAccessToken);
        assertTrue(userRest.sessionID!=null);

    }



    public Payment checkInitPaymentProcess() throws Exception{
        InitPaymentProcess initPaymentProcess = new InitPaymentProcess();
        Payment payment = initPaymentProcess.sendRequest("this_is_session_id","GROUPIDENT",0,"5875fac8d709da281208976d",10,
                                                "paypal", "ILS","pita");
        assertTrue(payment.transcationId!=null);
        assertTrue(payment.url!=null);
        return payment;
    }

    @Test
    public void checkPaymentStatus() throws Exception{
        Payment payment = checkInitPaymentProcess();
        GetPaymentStatus getPaymentStatus = new GetPaymentStatus();
        payment = getPaymentStatus.sendRequest("this_is_session_id", payment.transcationId);
        assertTrue(payment.paymentStatus == Payment.PaymentStatus.WAITING_FOR_VALIDATION);
    }
}