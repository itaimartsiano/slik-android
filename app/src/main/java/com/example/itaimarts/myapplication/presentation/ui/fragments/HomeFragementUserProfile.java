package com.example.itaimarts.myapplication.presentation.ui.fragments;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.os.Bundle;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.example.itaimarts.myapplication.R;

import com.example.itaimarts.myapplication.utils.ImageUtils;
import com.facebook.FacebookSdk;





public class HomeFragementUserProfile extends Fragment {



    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getActivity());

        View view = inflater.inflate(R.layout.fragment_home_user_profile,null);

        ImageView img1 = (ImageView) view.findViewById(R.id.user_profile_picture);
        Bitmap bm = BitmapFactory.decodeResource(getResources(),
                R.drawable.com_facebook_profile_picture_blank_portrait);
        Bitmap resized = Bitmap.createScaledBitmap(bm, 100, 100, true);
        Bitmap conv_bm = ImageUtils.getRoundedRectBitmap(resized);
        img1.setImageBitmap(conv_bm);


        return view;

    }



}
