package com.example.itaimarts.myapplication.presentation.presenters.impl;

import com.example.itaimarts.myapplication.domain.executor.Executor;
import com.example.itaimarts.myapplication.domain.executor.MainThread;
import com.example.itaimarts.myapplication.domain.interactors.AlreadyLoggedInInteractor;
import com.example.itaimarts.myapplication.domain.interactors.impl.AlreadyLoggedInInteractorImpl;
import com.example.itaimarts.myapplication.domain.repository.UserDetailsRepo;
import com.example.itaimarts.myapplication.presentation.presenters.AbstractPresenter;
import com.example.itaimarts.myapplication.presentation.presenters.SplashPresenter;
import com.facebook.FacebookSdk;

/**
 * Created by itai marts on 15/08/2016.
 */
public class SplashPresenterImpl extends AbstractPresenter implements SplashPresenter, AlreadyLoggedInInteractor.callback {

    private SplashPresenter.View mView;
    private UserDetailsRepo mUserDetailsRepo;

    public SplashPresenterImpl(Executor executor, MainThread mainThread, View view, UserDetailsRepo userDetailsRepo) {
        super(executor, mainThread);
        mView = view;
        mUserDetailsRepo = userDetailsRepo;
    }

    @Override
    public void checkIfAlreadyLoggedIn() {
        AlreadyLoggedInInteractor alreadyLoggedInInteractor = new AlreadyLoggedInInteractorImpl(
                mExecutor,
                mMainThread,
                this,
                mUserDetailsRepo) {
        };
        alreadyLoggedInInteractor.execute();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void loggedIn() {

        mView.showHomeActivity();
    }

    @Override
    public void notLoggedIn() {
        mView.showLoginActivity();
    }
}
