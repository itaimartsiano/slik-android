package com.example.itaimarts.myapplication.network.services;


import android.os.AsyncTask;
import android.util.Log;


import com.example.itaimarts.myapplication.domain.model.Product;
import com.example.itaimarts.myapplication.domain.model.Restaurant;
import com.example.itaimarts.myapplication.domain.model.Table;

import com.example.itaimarts.myapplication.domain.network.SynchTableService;
import com.example.itaimarts.myapplication.domain.repository.TableRepo;
import com.example.itaimarts.myapplication.domain.repository.UserDetailsRepo;
import com.example.itaimarts.myapplication.network.checkoutServer.GetTableBill;
import com.example.itaimarts.myapplication.network.checkoutServer.GetTableInfo;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by itai marts on 20/09/2016.
 * This class serve as our sync service, DONT use Android service platform
 * TODO: check if it is a vulnerability
 */


public class SynchTableServiceImpl implements com.example.itaimarts.myapplication.domain.network.SynchTableService {

    callback callback;
    ScheduledExecutorService scheduler;
    UserDetailsRepo userDetailsRepo;
    TableRepo tableRepo;
    Table lastResponse = null;
    GetTableInfo getTableInfo = new GetTableInfo();
    GetTableBill getTableBill = new GetTableBill();
    private final int timeToCheck = 25;

    @Override
    public void startSynch() {
        scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                checkForNewData();
            }
        }, 0, timeToCheck, TimeUnit.SECONDS);
        Log.d("SyncTableServiceImpl","started");
    }

    @Override
    public void stopSynch() {
        scheduler.shutdown();
    }

    @Override
    public void initService(com.example.itaimarts.myapplication.domain.network.SynchTableService.callback callback, UserDetailsRepo userDetailsRepo, TableRepo tableRepo) {
        this.callback = callback;
        this.userDetailsRepo = userDetailsRepo;
        this.tableRepo = tableRepo;
    }

    private void checkForNewData(){
        if (lastResponse == null){
            lastResponse = getTableInfo.getTableDetails(userDetailsRepo.getUserSessionID(),tableRepo.getTableID());
            callback.newDataFromServer(lastResponse.bill.products,lastResponse,lastResponse.restaurant);
        }else {
            Table newTableData = getTableBill.getTableBill(userDetailsRepo.getUserSessionID(),
                                                           lastResponse.tableID,
                                                           lastResponse.bill.version);
            //TODO: BUG - equals dont work here
            if (!lastResponse.equals(newTableData)){
                lastResponse = newTableData;
                callback.newDataFromServer(newTableData.bill.products,newTableData,newTableData.restaurant);
            }
        }
    }




    private ArrayList<Product> makeFakeProducts(){

        ArrayList<Product> products = new ArrayList<Product>();

        products.add(new Product("French Fries", 22, 3,0,"0"));
        products.add(new Product("Ceasar Salad", 38, 2,0,"1"));
        products.add(new Product("Cheeseburger", 49, 1,0,"2"));
        products.add(new Product("Toffu Salad", 39, 1,0,"3"));
        products.add(new Product("Toffu Salad", 39, 1,0,"4"));
        products.add(new Product("Toffu Salad", 39, 1,0,"5"));
        products.add(new Product("Toffu Salad", 39, 1,0,"6"));

        return products;
    }

    private Restaurant makeFakeRest(){
        return new Restaurant("Garage","fake info","fake","small","big");
    }

    private Table makeFakeTable(){
        return new Table(makeFakeProducts(),makeFakeRest(),257, 190, "ILS",1,"63287dsd");
    }

}
