    package com.example.itaimarts.myapplication.presentation.ui.activities;

import android.app.Activity;

import android.content.Intent;
import android.os.Bundle;

import com.example.itaimarts.myapplication.domain.executor.impl.ThreadExecutor;
import com.example.itaimarts.myapplication.presentation.animations.AnimationEnd;
import com.example.itaimarts.myapplication.R;
import com.example.itaimarts.myapplication.presentation.presenters.SplashPresenter;
import com.example.itaimarts.myapplication.presentation.presenters.impl.SplashPresenterImpl;
import com.example.itaimarts.myapplication.storage.UserDetailsRepoImpl;
import com.example.itaimarts.myapplication.threading.MainThreadImpl;


    public class SplashActivity extends Activity implements AnimationEnd, SplashPresenter.View {

    private SplashPresenter mSplashPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        init();
    }

    public void init(){
        mSplashPresenter = new SplashPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(),this, UserDetailsRepoImpl.getInstance());
    }

    @Override
    public void animationEnd() {
        mSplashPresenter.checkIfAlreadyLoggedIn();
    }

    @Override
    public void showHomeActivity() {
        Intent home = new Intent(getBaseContext(), HomeActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(home);
        this.finish();
    }

    @Override
    public void showLoginActivity() {
        Intent login = new Intent(getBaseContext(), LoginActivity.class);
        login.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(login);
        this.finish();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showError(String message) {

    }
    }
