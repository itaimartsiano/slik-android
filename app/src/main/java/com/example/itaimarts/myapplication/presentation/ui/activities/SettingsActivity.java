package com.example.itaimarts.myapplication.presentation.ui.activities;


import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.itaimarts.myapplication.R;
import com.example.itaimarts.myapplication.domain.repository.UserDetailsRepo;
import com.example.itaimarts.myapplication.storage.UserDetailsRepoImpl;
import com.example.itaimarts.myapplication.utils.ImageUtils;
import com.facebook.Profile;

public class SettingsActivity extends PreferenceActivity {

    UserDetailsRepo userDetailsRepo = UserDetailsRepoImpl.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();
        updateUIWithUserProfile();

    }


    public void updateUIWithUserProfile() {
        TextView email = (TextView) findViewById(R.id.user_email_textView);
        TextView userName = (TextView) findViewById(R.id.user_profile_textView);
        ImageView profilePicture = (ImageView) findViewById(R.id.user_profile_picture);
        email.setText(userDetailsRepo.getEmail());

        //TODO:need to get the user name from the fetchFacebookUserData
        userDetailsRepo.setUserName(Profile.getCurrentProfile().getName());
        userName.setText(userDetailsRepo.getUserName());

        Bitmap bitmap = userDetailsRepo.getProfilePicture();
        if (bitmap != null){
            bitmap = ImageUtils.getRoundedCroppedBitmap(bitmap);
            profilePicture.setImageBitmap(bitmap);
        }
    }



    void signOut() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra("finish", true);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
        startActivity(intent);
        finish();
    }

    public static class MyPreferenceFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);
        }
    }

}
