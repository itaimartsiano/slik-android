package com.example.itaimarts.myapplication.presentation.presenters;

import com.example.itaimarts.myapplication.presentation.ui.BaseView;
import com.facebook.AccessToken;

/**
 * Created by itai marts on 17/08/2016.
 */
public interface LoginPresenter {
    interface View {
        void showHomeActivity();
        void showLoginActivity();
        void showWaitingForCheckoutServerValidation();
    }

    void UserApprovedAppInFacebook(AccessToken accessToken);
}
