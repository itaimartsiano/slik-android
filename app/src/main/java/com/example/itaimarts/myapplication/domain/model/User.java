package com.example.itaimarts.myapplication.domain.model;

import android.graphics.Bitmap;

import java.util.Date;

/**
 * Created by itai marts on 13/06/2016.
 */
public class User {

    public Date birthday = null;
    public Bitmap picture = null;
    public String imageUrl = null;
    public String email = null;
    public String name = null;
    public String facebookToken = null;
    public String sessionID = null;

    public User(){}

    public User(Date birthday, Bitmap picture, String email, String name, String facebookToken, String sessionID) {
        this.birthday = birthday;
        this.picture = picture;
        this.email = email;
        this.name = name;
        this.facebookToken = facebookToken;
        this.sessionID = sessionID;
    }

    public User(String userId, String token) {
        this.sessionID = userId;
        this.facebookToken = token;
    }
    public User(Bitmap bitmap, String userName, String email){//TODO:correct the constructor like above
        this.picture = bitmap;
        this.name = userName;
        this.email = email;
    }


}
