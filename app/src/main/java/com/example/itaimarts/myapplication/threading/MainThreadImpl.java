package com.example.itaimarts.myapplication.threading;

import android.os.Handler;
import android.os.Looper;

import com.example.itaimarts.myapplication.domain.executor.MainThread;


/**
 * Created by itai marts on 14/08/2016.
 */
public class MainThreadImpl implements MainThread {

    private static MainThread sMainThread;

    private Handler mHandler;

    private MainThreadImpl() {
        mHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void post(Runnable runnable) {
        mHandler.post(runnable);
    }

    public static MainThread getInstance() {
        if (sMainThread == null) {
            sMainThread = new MainThreadImpl();
        }

        return sMainThread;
    }
}
