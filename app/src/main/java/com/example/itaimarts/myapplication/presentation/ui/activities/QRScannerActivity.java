package com.example.itaimarts.myapplication.presentation.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.example.itaimarts.myapplication.domain.executor.impl.ThreadExecutor;
import com.example.itaimarts.myapplication.domain.model.Table;
import com.example.itaimarts.myapplication.domain.repository.TableRepo;
import com.example.itaimarts.myapplication.presentation.presenters.QRScannerPresenter;
import com.example.itaimarts.myapplication.presentation.presenters.impl.QRScannerPresenterImpl;
import com.example.itaimarts.myapplication.storage.TableRepoImpl;
import com.example.itaimarts.myapplication.threading.MainThreadImpl;
import com.example.itaimarts.myapplication.utils.Tweaks;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

/**
 * Created by itai marts on 19/09/2016.
 */
public class QRScannerActivity extends Activity implements QRScannerPresenter.View {

    QRScannerPresenter qrScannerPresenter = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        qrScannerPresenter = new QRScannerPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, getApplicationContext());
        powerOnScanner();
        //TODO: when network with server available, replace the temp mission with QRCodeScanned method

    }

    public void powerOnScanner(){
        if(Tweaks.CAMERA_ENABLED){
            //QR scanner
            IntentIntegrator integrator = new IntentIntegrator(this);
            integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
            integrator.setPrompt("Scan Table QR");
            integrator.setCameraId(0);  // Use a specific camera of the device
            integrator.setBeepEnabled(true);
            integrator.setOrientationLocked(false);
            integrator.setBarcodeImageEnabled(true);
            integrator.initiateScan();
        }else {
            //TODO: replace with another task
            Intent orderDetails = new Intent(this, OrderDetailsActivity.class);
            orderDetails.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(orderDetails);
            TableRepoImpl.getInstance().setTableID(Tweaks.TABLE_SAMPLE_ID);
            this.finish();
        }
    }

    //Result from QR scanner
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        String QR = scanResult.getContents();
        qrScannerPresenter.QRScanned(QR);
    }



    @Override
    public void showOrderDetails() {
        Intent orderDetails = new Intent(this, OrderDetailsActivity.class);
        orderDetails.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(orderDetails);
        this.finish();
    }

    @Override
    public void errShowHome(String message) {
       //TODO: retrieve home screen from stack or inflate it again
    }



    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showError(String message) {

    }
}
