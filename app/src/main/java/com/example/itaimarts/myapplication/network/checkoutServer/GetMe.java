package com.example.itaimarts.myapplication.network.checkoutServer;

import com.example.itaimarts.myapplication.domain.model.User;

import com.example.itaimarts.myapplication.domain.network.GetUserDetailsEndPoint;
import com.example.itaimarts.myapplication.network.converters.ToDomainUser;
import com.example.itaimarts.myapplication.network.restTemplate.models.UserRest;

import retrofit.RestAdapter;

import retrofit.http.GET;
import retrofit.http.Header;

/**
 * Created by itai marts on 24/12/2016.
 */

public class GetMe extends EndPointBase implements GetUserDetailsEndPoint {

    protected GetMeApi registerUserApi;


    public GetMe(){
        restAdapter = new RestAdapter.Builder().setEndpoint(checkoutServer).build();
        registerUserApi = restAdapter.create(GetMeApi.class);
    }


    public User send(String sessionID){
        UserRest userRest = registerUserApi.getMe(sessionID);
        return  ToDomainUser.convert(userRest);
    }



    ////////////////////////////////////////////////////////////////
    //this is what the server support
    private interface GetMeApi {


        @GET("/users/me")
        UserRest getMe(@Header("x-session-id") String sessionID);

    }
}

