package com.example.itaimarts.myapplication.presentation.animations;

import android.widget.ImageView;

/**
 * Created by itai marts on 25/07/2016.
 */
public class Rotation {

    public static final int ROTATION_DURATION = 700;
    public static final int TRANSLATION_DURATION = 700;
    public static final int START_TRANSLATION_DELAY = ROTATION_DURATION+100;
    public static final int LOGO_DUARTION = 1500;
    public static final int LOGO_FADE_START_DELAY = START_TRANSLATION_DELAY + TRANSLATION_DURATION;


    public static void imageView(int angle, ImageView imageView){
        imageView.animate().rotation(angle).setDuration(ROTATION_DURATION).start();
    }

}
