package com.example.itaimarts.myapplication.network.checkoutServer;

import com.example.itaimarts.myapplication.domain.model.Product;
import com.example.itaimarts.myapplication.domain.model.Table;
import com.example.itaimarts.myapplication.network.converters.ToDomainTable;
import com.example.itaimarts.myapplication.network.restTemplate.models.GroupInfoDetailsRest;

import java.util.ArrayList;

import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Path;

/**
 * Created by itai marts on 24/12/2016.
 */

public class GetTableBill extends EndPointBase {

    protected GetGruopInfoByBillIndex getGruopInfoAPI;

    public GetTableBill(){
        restAdapter = new RestAdapter.Builder().setEndpoint(checkoutServer).build();
        getGruopInfoAPI = restAdapter.create(GetGruopInfoByBillIndex.class);
    }


    public Table getTableBill(String sessionID, String groupID, int billIndex){
        GroupInfoDetailsRest groupInfoDetailsRest = getGruopInfoAPI.getGroupInfo(sessionID, groupID, billIndex);
        //TODO:can get exception because of formatting in case of outdated
        return ToDomainTable.convert(groupInfoDetailsRest);
    }



    ////////////////////////////////////////////////////////////////

    //this is what the server support
    private interface GetGruopInfoByBillIndex {

        @GET("/groups/{group_id}/bills/{bill_index}")
        GroupInfoDetailsRest getGroupInfo(@Header("x-session-id") String sessionID,
                                          @Path("group_id") String groupID,
                                          @Path("bill_index") int billIndex);

    }

}

