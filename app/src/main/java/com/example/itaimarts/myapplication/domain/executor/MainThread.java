package com.example.itaimarts.myapplication.domain.executor;

/**
 * Created by itai marts on 14/08/2016.
 */
public interface MainThread {

    /**
     * Make runnable operation run in the main thread.
     *
     * @param runnable The runnable to run.
     */
    void post(final Runnable runnable);
}
