package com.example.itaimarts.myapplication.domain.interactors;

import com.example.itaimarts.myapplication.domain.interactors.base.Interactor;
import com.example.itaimarts.myapplication.domain.model.User;

/**
 * Created by itai marts on 17/08/2016.
 */
public interface GetPaymentURLInteractor extends Interactor{

    interface callback{
        public void paymentURLRequestSucceed(String url);
        public void paymentURLRequestfailed(String message);
    }

}
