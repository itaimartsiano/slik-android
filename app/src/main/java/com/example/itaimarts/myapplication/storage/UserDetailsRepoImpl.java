package com.example.itaimarts.myapplication.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;


import com.example.itaimarts.myapplication.domain.repository.UserDetailsRepo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Date;

/**
 * Created by itai marts on 16/08/2016.
 * TODO: take out the logic of saving data into user details data base, this is only the model.
 */
public class UserDetailsRepoImpl implements UserDetailsRepo {

    private static UserDetailsRepo userDetailsRepo;

    private Context mContext;
    private SharedPreferences sharedpreferences;

    private String userFacebookToken = null;
    private String userSessionID = null;
    private String userAppId = null;
    private String userName = null;
    private String email = null;
    private Bitmap profilePicture = null;
    private String imageUrl = null;
    private Date birthday = null;

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public static void init(Context context){
        userDetailsRepo = new UserDetailsRepoImpl(context);
    }

    public static UserDetailsRepo getInstance(){
        return userDetailsRepo;
    }

    private UserDetailsRepoImpl(){

    }

    private UserDetailsRepoImpl(Context context){
        mContext = context;
        sharedpreferences = context.getSharedPreferences("myPreferences", Context.MODE_PRIVATE);
    }


    public String getUserAppId() {
        return userAppId;
    }

    public void setUserAppId(String userAppId) {
        this.userAppId = userAppId;
    }


    public String getUserSessionID() {
        if (userSessionID == null){
            if (sharedpreferences.contains("userSessionID")) {
                this.userSessionID = sharedpreferences.getString("userSessionID",null);
            }
        }
        return userSessionID;
    }

    public void setUserSessionID(String userSessionID) {
        this.userSessionID = userSessionID;
        sharedpreferences.edit().putString("userSessionID", userSessionID).commit();
    }


    public String getUserFacebookToken() {
        if (userFacebookToken == null){
            if (sharedpreferences.contains("userFacebookToken")) {
                userFacebookToken = sharedpreferences.getString("userFacebookToken","notValid");
                Log.d("UserDetailsRepoImpl","getUserFacebookToken Was Called and got - "+ userFacebookToken);
            }
        }

        return userFacebookToken;
    }

    public void setUserFacebookToken(String userFacebookToken) {
        this.userFacebookToken = userFacebookToken;
        sharedpreferences.edit().putString("userFacebookToken", userFacebookToken).commit();
    }


    public String getUserName() {
        if (userName == null){
            userName = sharedpreferences.getString("user_name","notValid");
        }
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
        sharedpreferences.edit().putString("user_name", userName).commit();
    }


    public String getEmail() {
        if (email == null){
            email = sharedpreferences.getString("email","notValid");
        }
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        sharedpreferences.edit().putString("email", email).commit();
    }


    public void setProfilePicture(Bitmap bitmap) {
        this.profilePicture = bitmap;
        try {
            FileOutputStream out = mContext.openFileOutput("profilePicture.png", Context.MODE_PRIVATE);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.close();
            Log.d("UserDetailsRepoImpl","profile picture saved");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Bitmap getProfilePicture() {
        if (profilePicture == null){
            try {
                File filePath = mContext.getFileStreamPath("profilePicture.png");
                FileInputStream fi = new FileInputStream(filePath);
                profilePicture = BitmapFactory.decodeStream(fi);
                Log.d("UserDetailsRepoImpl","profile picture read from internal storage");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
         }
         return profilePicture;
        }


    public void logOut() {
        userFacebookToken = null;
        sharedpreferences.edit().remove("userFacebookToken").commit();
        sharedpreferences.edit().remove("userFacebookId").commit();
    }




}
