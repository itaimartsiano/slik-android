package com.example.itaimarts.myapplication.domain.interactors.impl;

import com.example.itaimarts.myapplication.domain.executor.Executor;
import com.example.itaimarts.myapplication.domain.executor.MainThread;
import com.example.itaimarts.myapplication.domain.interactors.LaunchSynchTableServiceInteractor;
import com.example.itaimarts.myapplication.domain.interactors.base.AbstractInteractor;
import com.example.itaimarts.myapplication.domain.network.SynchTableService;
import com.example.itaimarts.myapplication.domain.repository.ProductsRepo;
import com.example.itaimarts.myapplication.domain.repository.RestuarantRepo;
import com.example.itaimarts.myapplication.domain.repository.TableRepo;
import com.example.itaimarts.myapplication.domain.repository.UserDetailsRepo;

/**
 * Created by itai marts on 15/08/2016.
 */
public class LaunchSynchTableServiceInteractorImpl extends AbstractInteractor implements LaunchSynchTableServiceInteractor {

    private callback mCallback;
    private UserDetailsRepo mUserDetailsRepo;
    private ProductsRepo mProductsRepo;
    private RestuarantRepo mRestuarantRepo;
    private TableRepo mTableRepo;
    private SynchTableService mSynchTableService;
    private SynchTableService.callback msyncTableCallback;

    public LaunchSynchTableServiceInteractorImpl(Executor threadExecutor, MainThread mainThread, LaunchSynchTableServiceInteractor.callback mCallback,
                                                 UserDetailsRepo mUserDetailsRepo, ProductsRepo mProductsRepo, RestuarantRepo mRestuarantRepo,
                                                 TableRepo mTableRepo, SynchTableService synchTableService, SynchTableService.callback syncTableCallback) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mUserDetailsRepo = mUserDetailsRepo;
        this.mProductsRepo = mProductsRepo;
        this.mRestuarantRepo = mRestuarantRepo;
        this.mTableRepo = mTableRepo;
        this.mSynchTableService = synchTableService;
        this.msyncTableCallback = syncTableCallback;

    }



    @Override
    public void run() {
        mSynchTableService.initService(msyncTableCallback,mUserDetailsRepo,mTableRepo);
        mSynchTableService.startSynch();
    }


    @Override
    public SynchTableService getSyncService() {
        return mSynchTableService;
    }
}
