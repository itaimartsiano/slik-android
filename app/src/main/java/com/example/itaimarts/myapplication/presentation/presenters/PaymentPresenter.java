package com.example.itaimarts.myapplication.presentation.presenters;

import com.example.itaimarts.myapplication.presentation.ui.BaseView;

import java.net.URL;

/**
 * Created by itai marts on 15/08/2016.
 */
public interface PaymentPresenter extends BasePresenter {

    public void getPaymentURL(String paymentProvider, double amount);
    public void userPaymentSucceed(String paymentPaypalToken);
    public void userPaymentFailed(String message);

    interface View extends BaseView{
        public void showUserCheckoutPaymentScreen(String PaymentURL);
        public void showUserPaymentSucceedScreen();
    }

}
