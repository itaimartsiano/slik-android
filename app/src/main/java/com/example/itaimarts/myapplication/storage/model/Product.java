package com.example.itaimarts.myapplication.storage.model;

/**
 * Created by itai marts on 18/09/2016.
 */
public class Product {
    private String productName;
    private int ID;
    private int price;
    private int amount;
    private int selected;

    public Product(String productName, int price, int amount, int selected) {
        this.productName = productName;
        this.price = price;
        this.amount = amount;
        this.selected = selected;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }
}
