package com.example.itaimarts.myapplication.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;

import com.example.itaimarts.myapplication.domain.executor.Executor;
import com.example.itaimarts.myapplication.domain.executor.MainThread;
import com.example.itaimarts.myapplication.domain.interactors.LoginInteractor;
import com.example.itaimarts.myapplication.domain.interactors.impl.LoginInteractorImpl;
import com.example.itaimarts.myapplication.domain.network.LoginEndpoint;
import com.example.itaimarts.myapplication.network.checkoutServer.Login;
import com.example.itaimarts.myapplication.presentation.presenters.AbstractPresenter;
import com.example.itaimarts.myapplication.presentation.presenters.LoginPresenter;
import com.example.itaimarts.myapplication.storage.UserDetailsRepoImpl;
import com.facebook.AccessToken;

/**
 * Created by itai marts on 17/08/2016.
 */
public class LoginPresenterImpl extends AbstractPresenter implements LoginPresenter, LoginInteractor.callback{

    private Executor executor;
    private MainThread mainThread;
    private View view;
    private Context context;
    private String mAccessToken = null;


    public LoginPresenterImpl(Executor executor, MainThread mainThread, LoginPresenter.View view, Context context){
        super(executor,mainThread);
        this.executor = executor;
        this.mainThread = mainThread;
        this.view = view;
        this.context = context;
    }

    @Override
    public void UserApprovedAppInFacebook(AccessToken accessToken) {

        Log.d("LoginPresenterImpl","UserApprovedAppInFacebook");

        if (accessToken != null) {
            mAccessToken = accessToken.getToken();
            LoginEndpoint loginEndpoint = new Login();
            LoginInteractor loginInteractor = new LoginInteractorImpl(executor, mainThread, loginEndpoint,
                    this, accessToken.getToken(), accessToken.getUserId(), UserDetailsRepoImpl.getInstance());
            loginInteractor.execute();
        }else {
            //TODO: what to do if the acces token was null
        }

        view.showWaitingForCheckoutServerValidation();

    }

    @Override
    public void loginSuccess() {
        view.showHomeActivity();
    }

    @Override
    public void loginFailed() {
        view.showLoginActivity();
    }
}
