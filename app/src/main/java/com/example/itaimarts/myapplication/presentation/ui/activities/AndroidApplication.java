package com.example.itaimarts.myapplication.presentation.ui.activities;

import android.app.Application;
import com.example.itaimarts.myapplication.storage.ProductsRepoImpl;
import com.example.itaimarts.myapplication.storage.RestuarantRepoImpl;
import com.example.itaimarts.myapplication.storage.TableRepoImpl;
import com.example.itaimarts.myapplication.storage.UserDetailsRepoImpl;
import com.facebook.FacebookSdk;


/**
 * Created by dmilicic on 12/10/15.
 */
public class AndroidApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(this);
        UserDetailsRepoImpl.init(this);
        ProductsRepoImpl.init(this);
        RestuarantRepoImpl.init(this);
        TableRepoImpl.init(this);
    }

}
