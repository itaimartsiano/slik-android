package com.example.itaimarts.myapplication.domain.interactors;

import com.example.itaimarts.myapplication.domain.interactors.base.Interactor;

/**
 * Created by itai marts on 17/08/2016.
 */
public interface GetUserDInteractor extends Interactor{

    interface callback{
        void newDataSaved();
        void requestFailed();
    }

}
