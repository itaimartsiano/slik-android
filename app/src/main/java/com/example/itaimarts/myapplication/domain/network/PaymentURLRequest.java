package com.example.itaimarts.myapplication.domain.network;

import com.example.itaimarts.myapplication.domain.model.Payment;
import com.example.itaimarts.myapplication.domain.model.Product;
import com.example.itaimarts.myapplication.domain.model.Restaurant;
import com.example.itaimarts.myapplication.domain.model.Table;
import com.example.itaimarts.myapplication.domain.repository.TableRepo;
import com.example.itaimarts.myapplication.domain.repository.UserDetailsRepo;

import java.util.ArrayList;

/**
 * Created by itai marts on 17/08/2016.
 */
public interface PaymentURLRequest {

    public Payment sendRequest(String sessionID, String groupID, int billIndex,
                               String buisnessID, int total, String vendor,
                               String currency, String description);

}
