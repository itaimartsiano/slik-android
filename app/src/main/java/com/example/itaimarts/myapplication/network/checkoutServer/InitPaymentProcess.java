package com.example.itaimarts.myapplication.network.checkoutServer;

import com.example.itaimarts.myapplication.domain.model.Payment;
import com.example.itaimarts.myapplication.domain.network.PaymentURLRequest;
import com.example.itaimarts.myapplication.network.converters.ToDomainPayment;
import com.example.itaimarts.myapplication.network.restTemplate.models.InitPaymentResponseRest;

import retrofit.RestAdapter;
import retrofit.http.Body;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by itai marts on 24/12/2016.
 */


//TODO: need to fix - not working
public class InitPaymentProcess extends EndPointBase implements PaymentURLRequest {

    protected payAPI payAPI;

    public InitPaymentProcess(){
        restAdapter = new RestAdapter.Builder().setEndpoint(checkoutServer).build();
        payAPI = restAdapter.create(payAPI.class);
    }


    public Payment sendRequest(String sessionID, String groupID, int billIndex,
                               String buisnessID, int total, String vendor,
                               String currency, String description){
    //TODO:change return value
        BodyFormat bodyFormat = new BodyFormat();
        bodyFormat.bill = billIndex;
        bodyFormat.group = groupID;
        bodyFormat.payment_description.currency = currency;
        bodyFormat.payment_description.total = total;
        bodyFormat.payment_description.vendor = vendor;
        bodyFormat.payment_description.description = description;

        InitPaymentResponseRest serverResponse = payAPI.payToBuisness(sessionID, buisnessID, bodyFormat);
        return ToDomainPayment.convert(serverResponse);
    }



    ////////////////////////////////////////////////////////////////

    private class BodyFormat {

        private String group;
        private int bill;
        private PaymentDescription payment_description;

        public BodyFormat(){
            this.payment_description = new PaymentDescription();
        }

        public class PaymentDescription{
            public String vendor;
            public int total;
            public String currency;
            public String description;
        }

    }

    //this is what the server support
    private interface payAPI {

        @POST("/businesses/{business_id}/pay")
        InitPaymentResponseRest payToBuisness(@Header("x-session-id") String sessionID,
                                              @Path("business_id") String buisnessID,
                                              @Body BodyFormat bodyFormat);
    }

}

