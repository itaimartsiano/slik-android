package com.example.itaimarts.myapplication.domain.network;


import com.example.itaimarts.myapplication.domain.model.User;

/**
 * Created by itai marts on 17/08/2016.
 */
public interface GetUserDetailsEndPoint {

    public User send(String sessionID);

}
