package com.example.itaimarts.myapplication.network.checkoutServer;

import android.util.Log;

import com.example.itaimarts.myapplication.domain.model.User;
import com.example.itaimarts.myapplication.domain.network.LoginEndpoint;
import com.example.itaimarts.myapplication.network.restTemplate.models.UserRest;

import retrofit.RestAdapter;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by itai marts on 24/12/2016.
 */

public class Login extends EndPointBase implements LoginEndpoint {

    protected LoginApi loginApi;

    public Login(){
        restAdapter = new RestAdapter.Builder().setEndpoint(checkoutServer).build();
        loginApi = restAdapter.create(LoginApi.class);
    }


    public User send(String facebookToken){
        Log.d("endpoint","try to connect server - login request");
        BodyFormat bodyFormat = new BodyFormat();
        bodyFormat.fb_access_token = facebookToken;
        UserRest userRest = loginApi.login(bodyFormat);
        return  convert(userRest);
    }


    private User convert(UserRest userRest){
        User user = new User();
        if (userRest != null){
            user.sessionID = userRest.session_id;
        }
        return user;
    }


    ////////////////////////////////////////////////////////////////
    private class BodyFormat {
        public String fb_access_token;
    }

    //this is what the server support
    private interface LoginApi {

        @POST("/users/login")
        UserRest login(@Body BodyFormat bodyParams);

    }

}

