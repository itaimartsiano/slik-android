package com.example.itaimarts.myapplication.presentation.ui.fragments;


import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;

import com.example.itaimarts.myapplication.presentation.animations.AnimationEnd;
import com.example.itaimarts.myapplication.R;
import com.example.itaimarts.myapplication.presentation.animations.FadeIn;
import com.example.itaimarts.myapplication.presentation.animations.Rotation;
import com.example.itaimarts.myapplication.presentation.animations.Translation;


public class SplashFragement extends Fragment {

    AnimationEnd mCallback;

    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        View view = null;
        view = inflater.inflate(R.layout.fragment_splash_start, container, false);

        //animate new user screen
       animateSplash(view);

        return view;

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception

        try {
            mCallback = (AnimationEnd) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement AnimationEnd");
        }
    }




    //this method responsible to animate the fragment screen for user who didn't log in yet
    private void animateSplash(View view){
        ImageView logoInCircle = (ImageView) view.findViewById(R.id.logo_in);
        ImageView logoOutCircle = (ImageView) view.findViewById(R.id.logo_out);
        ImageView textLogo = (ImageView) view.findViewById(R.id.logo_text);
        ImageView textLogo_end = (ImageView) view.findViewById(R.id.logo_text_end);


        Rotation.imageView(0, logoInCircle);
        Rotation.imageView(-360, logoOutCircle);
        Translation.imageView(-525,logoInCircle);
        Translation.imageView(-525, logoOutCircle);
        FadeIn.imageViewWithCallback(textLogo, mCallback);



    }

}
