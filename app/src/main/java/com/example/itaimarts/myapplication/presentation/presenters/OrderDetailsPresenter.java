package com.example.itaimarts.myapplication.presentation.presenters;

import com.example.itaimarts.myapplication.presentation.ui.BaseView;

import java.io.Serializable;

/**
 * Created by itai marts on 22/08/2016.
 */
public interface OrderDetailsPresenter extends BasePresenter {

    public void getTableInfo();
    public void payBtnPressed();
    public void checkoutPayment(String provider);
    public void finishPresenter();


    interface View extends BaseView{
        public void updateUiFromRepo();
        public void showCalcToUser();
        public void showPaymentActivity(String provider);
    }

}
