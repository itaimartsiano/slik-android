package com.example.itaimarts.myapplication.domain.model;

import com.example.itaimarts.myapplication.network.restTemplate.models.GroupInfoDetailsRest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by itai marts on 04/01/2017.
 */

public class Bill {

    public int version;
    public String id;
    public ArrayList <Product> products;
    public int totalSum;
    public int totalPayed;
    public String currency;
}
