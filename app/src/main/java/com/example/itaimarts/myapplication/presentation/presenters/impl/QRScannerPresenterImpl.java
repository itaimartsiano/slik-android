package com.example.itaimarts.myapplication.presentation.presenters.impl;

import android.content.Context;

import com.example.itaimarts.myapplication.domain.executor.Executor;
import com.example.itaimarts.myapplication.domain.executor.MainThread;
import com.example.itaimarts.myapplication.presentation.presenters.AbstractPresenter;
import com.example.itaimarts.myapplication.presentation.presenters.HomePresenter;
import com.example.itaimarts.myapplication.presentation.presenters.QRScannerPresenter;
import com.example.itaimarts.myapplication.storage.TableRepoImpl;


/**
 * Created by itai marts on 22/08/2016.
 */
public class QRScannerPresenterImpl extends AbstractPresenter implements QRScannerPresenter {

    private Context context;
    private View qrScannerActivity;

    public QRScannerPresenterImpl(Executor executor, MainThread mainThread,
                                  View qrScannerActivity, Context context) {
        super(executor, mainThread);
        this.context = context;
        this.qrScannerActivity = qrScannerActivity;
    }


    @Override
    public void QRScanned(String QR) {
        //weak check, elaborate it
        //in the future maybe we will need to call INTERACTOR class
        if (QR != null){
            int version = -1;
            TableRepoImpl.getInstance().setVersion(version);
            TableRepoImpl.getInstance().setTableID(QR);
            qrScannerActivity.showOrderDetails();
        }else {
            //change to user friendly message
            qrScannerActivity.errShowHome("QR is null");
        }
    }
}
