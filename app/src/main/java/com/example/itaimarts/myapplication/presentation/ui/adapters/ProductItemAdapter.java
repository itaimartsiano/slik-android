package com.example.itaimarts.myapplication.presentation.ui.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;


import com.example.itaimarts.myapplication.R;
import com.example.itaimarts.myapplication.domain.model.Product;
import com.example.itaimarts.myapplication.presentation.ui.activities.OrderDetailsActivity;
import com.example.itaimarts.myapplication.presentation.ui.fragments.OrderDetailsMainTabFragement;

import java.util.ArrayList;

/**
 * Created by itai marts on 17/09/2016.
 */
public class ProductItemAdapter extends ArrayAdapter<Product> {

    OrderDetailsMainTabFragement orderDetailsMainTabFragement;

    public ProductItemAdapter(Context context, ArrayList<Product> products, OrderDetailsMainTabFragement orderDetailsMainTabFragement) {
        super(context, 0, products);
        this.orderDetailsMainTabFragement = orderDetailsMainTabFragement;
    }


    public View getView(int position, View convertView, ViewGroup parent) {

        // Get the data item for this position
        Product product = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.product_details_item, parent, false);
        }

        // Lookup view for data population
        TextView productName = (TextView) convertView.findViewById(R.id.product_name);
        TextView productPrice = (TextView) convertView.findViewById(R.id.product_price);
        TextView productAmount = (TextView) convertView.findViewById(R.id.choice_numbers);


        // Populate the data into the template view using the data object
        productName.setText(product.getProductName());
        productPrice.setText((int)product.getPrice()+"  ILS");
        productAmount.setText(product.getSelected()+"/"+product.getAmount());

        setButtonsActions(convertView, product);

        // Return the completed view to render on screen
        return convertView;
    }



    public void setButtonsActions(final View view, final Product product) {
        Button btnMinus = (Button) view.findViewById(R.id.button_minus);
        Button btnPlus = (Button) view.findViewById(R.id.button_plus);
        final TextView productSelected = (TextView) view.findViewById(R.id.choice_numbers);
        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minusPressed(product, productSelected);
                orderDetailsMainTabFragement.updateProgressBarWithPrice();
            }
        });

        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plusPressed(product,productSelected);
                orderDetailsMainTabFragement.updateProgressBarWithPrice();
            }
        });
    }


    private void minusPressed(Product product, TextView productSelected){
        int lastSelected = product.getSelected();
        int amount = product.getAmount();
        if (product.getSelected()>0){
            product.setSelected(lastSelected-1);
            productSelected.setText((lastSelected-1)+"/"+amount);
        }
    }

    private void plusPressed(Product product, TextView productSelected){
        int lastSelected = product.getSelected();
        int amount = product.getAmount();
        if (lastSelected<amount){
            product.setSelected(lastSelected+1);
            productSelected.setText((lastSelected+1)+"/"+amount);
        }
    }


}
