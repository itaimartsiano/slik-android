package com.example.itaimarts.myapplication.storage.model;

/**
 * Created by itai marts on 19/09/2016.
 */
public class Restaurant {

    private String name;
    private String info;
    private String address;
    private String smallPictureLink;
    private String bigPictureLink;

    public Restaurant(String name, String info, String address, String smallPictureLink, String bigPictureLink) {
        this.name = name;
        this.info = info;
        this.address = address;
        this.smallPictureLink = smallPictureLink;
        this.bigPictureLink = bigPictureLink;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSmallPictureLink() {
        return smallPictureLink;
    }

    public void setSmallPictureLink(String smallPictureLink) {
        this.smallPictureLink = smallPictureLink;
    }

    public String getBigPictureLink() {
        return bigPictureLink;
    }

    public void setBigPictureLink(String bigPictureLink) {
        this.bigPictureLink = bigPictureLink;
    }
}
