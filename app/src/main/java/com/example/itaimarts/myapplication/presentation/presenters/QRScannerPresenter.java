package com.example.itaimarts.myapplication.presentation.presenters;

import com.example.itaimarts.myapplication.presentation.ui.BaseView;

/**
 * Created by itai marts on 22/08/2016.
 */
public interface QRScannerPresenter {

    public void QRScanned(String QR);


    interface View extends BaseView{
        public void showOrderDetails();
        public void errShowHome(String message);

    }

}
