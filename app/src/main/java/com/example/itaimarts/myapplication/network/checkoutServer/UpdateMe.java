package com.example.itaimarts.myapplication.network.checkoutServer;

import com.example.itaimarts.myapplication.domain.model.User;
import com.example.itaimarts.myapplication.network.restTemplate.models.UserRest;

import retrofit.RestAdapter;
import retrofit.http.Body;
import retrofit.http.Header;
import retrofit.http.POST;

/**
 * Created by itai marts on 24/12/2016.
 */

public class UpdateMe extends EndPointBase {

    protected UpdateMeApi registerUserApi;

    public UpdateMe(){
        restAdapter = new RestAdapter.Builder().setEndpoint(checkoutServer).build();
        registerUserApi = restAdapter.create(UpdateMeApi.class);
    }


    public User updateMe(String sessionID, String name, String image){
        BodyFormat bodyFormat = new BodyFormat();
        bodyFormat.name = name;
        bodyFormat.image = image;
        UserRest userRest = registerUserApi.updateMe(sessionID, bodyFormat);
        return  convert(userRest);
    }


    private User convert(UserRest userRest){
        User user = new User();
        if (userRest != null){
            user.birthday = userRest.birthday;
            user.imageUrl = userRest.picture;
            user.email = userRest.email;
            user.name = userRest.name;
        }
        return user;
    }


    ////////////////////////////////////////////////////////////////

    private class BodyFormat {
        public String name;
        public String image;
    }

    //this is what the server support
    private interface UpdateMeApi {


        @POST("/users/me")
        UserRest updateMe(@Header("x-session-id") String sessionID,
                          @Body BodyFormat bodyParams);

    }

}

