package com.example.itaimarts.myapplication.network.converters;

import com.example.itaimarts.myapplication.domain.model.User;
import com.example.itaimarts.myapplication.network.restTemplate.models.UserRest;

/**
 * Created by itai marts on 17/01/2017.
 */

public class ToDomainUser {

    public static User convert(UserRest userRest){
        User user = new User();
        if (userRest != null){
            user.birthday = userRest.birthday;
            user.imageUrl = userRest.picture;
            user.email = userRest.email;
            user.name = userRest.name;
            user.sessionID = userRest.session_id;
        }
        return user;
    }

}
