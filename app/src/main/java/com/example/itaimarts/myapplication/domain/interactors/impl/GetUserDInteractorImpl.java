package com.example.itaimarts.myapplication.domain.interactors.impl;

import android.graphics.Bitmap;
import android.util.Log;

import com.example.itaimarts.myapplication.domain.executor.Executor;
import com.example.itaimarts.myapplication.domain.executor.MainThread;
import com.example.itaimarts.myapplication.domain.interactors.GetUserDInteractor;
import com.example.itaimarts.myapplication.domain.interactors.base.AbstractInteractor;
import com.example.itaimarts.myapplication.domain.model.User;
import com.example.itaimarts.myapplication.domain.network.GetUserDetailsEndPoint;
import com.example.itaimarts.myapplication.domain.repository.UserDetailsRepo;
import com.example.itaimarts.myapplication.utils.ImageUtils;

/**
 * Created by itai marts on 17/08/2016.
 */
public class GetUserDInteractorImpl extends AbstractInteractor implements GetUserDInteractor {

    private GetUserDetailsEndPoint getUserDetails;
    private callback activityPresenter;

    private UserDetailsRepo userDetailsRepo;

    public GetUserDInteractorImpl(Executor threadExecutor, MainThread mainThread, GetUserDetailsEndPoint getUser,
                                  callback activityPresenter, UserDetailsRepo userDetailsRepo) {
        super(threadExecutor, mainThread);
        this.getUserDetails = getUser;
        this.activityPresenter = activityPresenter;
        this.userDetailsRepo = userDetailsRepo;
    }

    @Override
    public void run() {
        try {
            User user = getUserDetails.send(userDetailsRepo.getUserSessionID());
            onSuccessServerAns(user);
        }catch (Exception e){
            Log.e("",e.toString());
            onFailureServerAns("");
        }

    }



    public void onSuccessServerAns(User user) {

        if(hasNewUpdates(user))
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                activityPresenter.newDataSaved();
            }
        });

    }

    private boolean hasNewUpdates(User user) {
        userDetailsRepo.setBirthday(user.birthday);
        userDetailsRepo.setImageUrl(user.imageUrl);
        Bitmap image = ImageUtils.DownloadImageFromURL(user.imageUrl);
        userDetailsRepo.setProfilePicture(ImageUtils.getRoundedCroppedBitmap(image));
        userDetailsRepo.setEmail(user.email);
        userDetailsRepo.setUserName(user.name);
        //TODO: add history updtae
        //TODO: need to think how to make comprasion better, and only if diffrent creturn true;
        return true;
    }


    public void onFailureServerAns(String message) {
        activityPresenter.requestFailed();
    }
}
