package com.example.itaimarts.myapplication.network.checkoutServer;

import com.example.itaimarts.myapplication.domain.model.Table;
import com.example.itaimarts.myapplication.network.converters.ToDomainTable;
import com.example.itaimarts.myapplication.network.restTemplate.models.GroupInfoDetailsRest;
import com.example.itaimarts.myapplication.utils.Tweaks;

import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Path;

/**
 * Created by itai marts on 24/12/2016.
 */

public class GetTableInfo extends EndPointBase {

    protected GetGruopInfoAPI getGruopInfoAPI;

    public GetTableInfo(){
        restAdapter = new RestAdapter.Builder().setEndpoint(checkoutServer).build();
        getGruopInfoAPI = restAdapter.create(GetGruopInfoAPI.class);
    }


    public Table getTableDetails(String sessionID, String groupID){
        GroupInfoDetailsRest groupInfoDetailsRest = getGruopInfoAPI.getGroupInfo(sessionID, groupID);
        return  ToDomainTable.convert(groupInfoDetailsRest);
    }





    ////////////////////////////////////////////////////////////////

    //this is what the server support
    private interface GetGruopInfoAPI {

        @GET("/groups/{id}")
        GroupInfoDetailsRest getGroupInfo(@Header("x-session-id") String sessionID,
                                          @Path("id") String groupID);

    }

}

