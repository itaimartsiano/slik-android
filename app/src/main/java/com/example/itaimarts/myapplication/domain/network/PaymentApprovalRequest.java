package com.example.itaimarts.myapplication.domain.network;

import com.example.itaimarts.myapplication.domain.interactors.LoginInteractor;

/**
 * Created by itai marts on 17/08/2016.
 */
public interface PaymentApprovalRequest {

    public void send(String approvalToken, String paymentProvider, Callback interactor);

    interface Callback{
        public void paymentApproved();
        public void paymentNotApproved(String message);
    }

}
