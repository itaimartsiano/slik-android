package com.example.itaimarts.myapplication.domain.interactors;

import com.example.itaimarts.myapplication.domain.interactors.base.Interactor;

/**
 * Created by itai marts on 17/08/2016.
 */
public interface GetServerPaymentApprovalInteractor extends Interactor{

    interface Callback{
        public void PaymentApprovedByServer();
        public void PaymentDeniedByServer(String message);
    }

}
