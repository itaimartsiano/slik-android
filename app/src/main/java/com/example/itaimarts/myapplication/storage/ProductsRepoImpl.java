package com.example.itaimarts.myapplication.storage;

import android.content.Context;
import android.content.SharedPreferences;


import com.example.itaimarts.myapplication.domain.model.Product;
import com.example.itaimarts.myapplication.domain.repository.ProductsRepo;

import java.util.ArrayList;

/**
 * Created by itai marts on 19/09/2016.
 */
public class ProductsRepoImpl implements ProductsRepo {

    private static ProductsRepo repoInstance = null;
    private SharedPreferences sharedpreferences;

    private ArrayList<Product> arrayList;



    private ProductsRepoImpl(Context context){
        sharedpreferences = context.getSharedPreferences("Products", Context.MODE_PRIVATE);
    }

    public static void init(Context context){
        if (repoInstance==null){
            repoInstance = new ProductsRepoImpl(context);
        }
    }

    public static ProductsRepo getInstance(){
        return repoInstance;
    }

    @Override
    public void insertProduct(com.example.itaimarts.myapplication.domain.model.Product product){
        //TODO convert to storage model
        if (!arrayList.contains(product)){
            arrayList.add(product);
        }
    }

    @Override
    public void deleteProduct(com.example.itaimarts.myapplication.domain.model.Product product){
        //TODO convert to storage model
        if (arrayList.contains(product)){
            arrayList.remove(product);
        }
    }

    @Override
    public ArrayList<Product> getProductsAsArrayList(){
        return arrayList;
    }

    @Override
    public void setProductsAsArrayList(ArrayList<Product> products) {
        //TODO change the models if needed (now we are working only on the domain models
        //TODO save in Local storage
        this.arrayList = products;

    }

    @Override
    public void clearData(){
        arrayList.clear();
    }

    @Override
    public void replaceProduct(com.example.itaimarts.myapplication.domain.model.Product oldProduct,
                               com.example.itaimarts.myapplication.domain.model.Product newProduct){
        //TODO convert to storage model
        if (arrayList.contains(oldProduct)){
            arrayList.remove(oldProduct);
            arrayList.add(newProduct);
        }
    }


}
