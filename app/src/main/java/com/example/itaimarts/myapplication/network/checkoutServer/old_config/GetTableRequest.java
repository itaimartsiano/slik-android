package com.example.itaimarts.myapplication.network.checkoutServer.old_config;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by itai marts on 21/09/2016.
 */

public class GetTableRequest {

    private static String path = "http://";

    public static String SendRequst(int version, String sessionID) throws IOException {

        JSONObject messageToServer = new JSONObject();
        try {
            messageToServer.put("version", version);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        URL serverUrl = new URL(path);
        HttpURLConnection connection = (HttpURLConnection) serverUrl.openConnection();
        connection.setDoOutput(true);
        connection.setRequestProperty("sessionID", sessionID);

        return sendMessage(messageToServer.toString(), connection);
    }



    private static String sendMessage(String messageToServer, HttpURLConnection connection) {

        InputStream inputStream = null;
        StringBuilder sb = new StringBuilder();

        try {

            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());

            //write the message to server
            writer.write(messageToServer, 0, messageToServer.length());
            writer.flush();
            writer.close();

            //get the ans from server
            inputStream = connection.getInputStream();
            char []  messageFromServer = new char[1024];

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            int read;
            while ((read = reader.read(messageFromServer)) > 0) {
                sb.append(messageFromServer, 0, read);
            }
        }
        catch (Exception e)
        {
            //TODO add log
        }

        finally
        {
            if (connection != null) {
                connection.disconnect();
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();
    }


}
