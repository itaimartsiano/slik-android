package com.example.itaimarts.myapplication.presentation.animations;

import android.animation.Animator;
import android.widget.Button;
import android.widget.ImageView;



/**
 * Created by itai marts on 25/07/2016.
 */


public class FadeIn {

    //TODO: reorganize animations time here and in SplashFragement.java
    public static final int ROTATION_DURATION = 700;
    public static final int TRANSLATION_DURATION = 700;
    public static final int START_TRANSLATION_DELAY = ROTATION_DURATION+100;
    public static final int LOGO_DUARTION = 500;
    public static final int LOGO_FADE_START_DELAY = START_TRANSLATION_DELAY + TRANSLATION_DURATION;


    public static void button(Button btn){
        btn.animate().alpha(1).setDuration(LOGO_DUARTION).setStartDelay(LOGO_FADE_START_DELAY);

    }

    public static void imageViewWithCallback(ImageView imageView, final AnimationEnd mCallback){
        imageView.animate().alpha(1).setDuration(LOGO_DUARTION).setStartDelay(LOGO_FADE_START_DELAY).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

                mCallback.animationEnd();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }




}
