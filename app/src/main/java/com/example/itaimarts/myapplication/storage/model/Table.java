package com.example.itaimarts.myapplication.storage.model;

import java.util.ArrayList;

/**
 * Created by itai marts on 19/09/2016.
 */
public class Table {

    private ArrayList<Product> arrayList;
    private Restaurant restaurant;
    private int totalSum;
    private int totalPayed;
    private String currency;
    private String version;

    public Table(ArrayList<Product> arrayList, Restaurant restaurant, int totalSum, int totalPayed, String currency, String version) {
        this.arrayList = arrayList;
        this.restaurant = restaurant;
        this.totalSum = totalSum;
        this.totalPayed = totalPayed;
        this.currency = currency;
        this.version = version;
    }

    public ArrayList<Product> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<Product> arrayList) {
        this.arrayList = arrayList;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public int getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(int totalSum) {
        this.totalSum = totalSum;
    }

    public int getTotalPayed() {
        return totalPayed;
    }

    public void setTotalPayed(int totalPayed) {
        this.totalPayed = totalPayed;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
