package com.example.itaimarts.myapplication.network.checkoutServer.old_config;

import com.example.itaimarts.myapplication.domain.interactors.LoginInteractor;

/**
 * Created by itai marts on 17/08/2016.
 */
public class LoginRequest extends AsyncRequest {

    private LoginInteractor loginInteractor;
    private String userId;


    public void SendRequest(LoginInteractor loginInteractor, String token, String userId) {
        this.loginInteractor = loginInteractor;
        this.userId = userId;
        execute();
    }


    public void onResponse() {
        //understand if success or fail
        //convert data to user model
        //loginInteractor.onFailure();
        //loginInteractor.onSuccess();

    }

}
