package com.example.itaimarts.myapplication.presentation.presenters.impl;

import android.content.Context;


import com.example.itaimarts.myapplication.domain.executor.Executor;
import com.example.itaimarts.myapplication.domain.executor.MainThread;
import com.example.itaimarts.myapplication.domain.interactors.GetPaymentURLInteractor;

import com.example.itaimarts.myapplication.domain.interactors.GetServerPaymentApprovalInteractor;
import com.example.itaimarts.myapplication.domain.interactors.impl.GetPaymentURLInteractorImpl;

import com.example.itaimarts.myapplication.domain.interactors.impl.GetServerPaymentApprovalInteractorImpl;
import com.example.itaimarts.myapplication.domain.network.PaymentApprovalRequest;
import com.example.itaimarts.myapplication.domain.network.PaymentURLRequest;

import com.example.itaimarts.myapplication.network.checkoutServer.InitPaymentProcess;

import com.example.itaimarts.myapplication.network.checkoutServer.old_config.PaymentApprovalRequestImpl;
import com.example.itaimarts.myapplication.presentation.presenters.AbstractPresenter;

import com.example.itaimarts.myapplication.presentation.presenters.PaymentPresenter;

/**
 * Created by itai marts on 22/08/2016.
 */

public class PaymentPresenterImpl extends AbstractPresenter implements PaymentPresenter, GetPaymentURLInteractor.callback, GetServerPaymentApprovalInteractor.Callback {

    private Context context;
    private View paymentActivity;
    private String paymentProvider;

    public PaymentPresenterImpl(Executor executor, MainThread mainThread,
                                View orderDetailsActivity, Context context) {
        super(executor, mainThread);
        this.context = context;
        this.paymentActivity = orderDetailsActivity;
    }

    @Override
    public void getPaymentURL(String paymentProvider, double amount) {
        this.paymentProvider = paymentProvider;
        PaymentURLRequest paymentURLRequest = new InitPaymentProcess();
        GetPaymentURLInteractor getPaymentURLInteractor = new GetPaymentURLInteractorImpl(mExecutor,mMainThread, amount, paymentURLRequest,this);
        getPaymentURLInteractor.execute();
    }

    @Override
    public void paymentURLRequestSucceed(String url) {
        paymentActivity.showUserCheckoutPaymentScreen(url);
    }

    @Override
    public void paymentURLRequestfailed(String message) {
        //TODO: implement failed
    }


    @Override
    public void userPaymentSucceed(String paymentPaypalToken) {
        PaymentApprovalRequest paymentApprovalRequestEndPoint = new PaymentApprovalRequestImpl();
        GetServerPaymentApprovalInteractor getServerPaymentApprovalInteractor
                = new GetServerPaymentApprovalInteractorImpl(mExecutor,mMainThread,paymentProvider,
                                                            paymentPaypalToken, paymentApprovalRequestEndPoint,
                                                            this);
        getServerPaymentApprovalInteractor.execute();
    }

    @Override
    public void userPaymentFailed(String message) {
        //TODO: implement payment failed
    }

    @Override
    public void PaymentApprovedByServer() {
        paymentActivity.showUserPaymentSucceedScreen();
    }

    @Override
    public void PaymentDeniedByServer(String message) {
        //TODO:implement
    }

    /*
    Not implemented yet ------------------------------------------------------
     */

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }


}
