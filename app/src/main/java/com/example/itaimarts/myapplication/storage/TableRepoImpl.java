package com.example.itaimarts.myapplication.storage;


import android.content.Context;
import android.content.SharedPreferences;


/**
 * Created by itai marts on 19/09/2016.
 */
public class TableRepoImpl implements com.example.itaimarts.myapplication.domain.repository.TableRepo {


    private static TableRepoImpl repoInstance = null;
    private SharedPreferences sharedpreferences;

    private String tableID;
    private int totalOrderSum;
    private int totalPayed;
    private int totalRequestSum;
    private int tipPrecentageRequest;
    private String currency = "ILS";
    private int version = -1;



    private TableRepoImpl(Context context){
        sharedpreferences = context.getSharedPreferences("Table", Context.MODE_PRIVATE);
    }

    public static void init(Context context){
        if (repoInstance==null){
            repoInstance = new TableRepoImpl(context);
        }
    }

    public static TableRepoImpl getInstance(){
        return repoInstance;
    }

    @Override
    public String getTableID() {
        if (tableID==null && sharedpreferences.contains("Table_ID")){
            tableID = sharedpreferences.getString("Table_ID",null);
        }
        return tableID;
    }

    @Override
    public void setTableID(String tableID) {
        sharedpreferences.edit().putString("Table_ID", tableID).commit();
        this.tableID = tableID;
    }

    @Override
    public int getVersion() {
        if (version==-1 && sharedpreferences.contains("Version")){
            version = sharedpreferences.getInt("Version",-1);
        }
        return version;
    }

    @Override
    public void setVersion(int version) {
        sharedpreferences.edit().putInt("Version", version).commit();
        this.version = version;
    }


    public int getTotalOrderSum() {
        return totalOrderSum;
    }

    public void setTotalOrderSum(int totalOrderSum) {
        this.totalOrderSum = totalOrderSum;
    }

    public int getTotalPayed() {
        return totalPayed;
    }

    public void setTotalPayed(int totalPayed) {
        this.totalPayed = totalPayed;
    }

    public int getTotalRequestSum() {
        return totalRequestSum;
    }

    public void setTotalRequestSum(int totalRequestSum) {
        this.totalRequestSum = totalRequestSum;
    }

    public int getTipPrecentageRequest() {
        return tipPrecentageRequest;
    }

    public void setTipPrecentageRequest(int tipPrecentageRequest) {
        this.tipPrecentageRequest = tipPrecentageRequest;
    }

    @Override
    public String getCurrency() {
        return currency;
    }

    @Override
    public void setCurrency(String currency) {
        this.currency = currency;
    }





}
