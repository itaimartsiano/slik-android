package com.example.itaimarts.myapplication.domain.interactors;

import com.example.itaimarts.myapplication.domain.interactors.base.Interactor;
import com.example.itaimarts.myapplication.domain.network.SynchTableService;

/**
 * Created by itai marts on 15/08/2016.
 */
public interface LaunchSynchTableServiceInteractor extends Interactor{

    public SynchTableService getSyncService();

    interface callback{
        void serviceLaunchedSuccesfully();
        void serviceLaunchFailed();
    }

}
