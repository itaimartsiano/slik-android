package com.example.itaimarts.myapplication.domain.interactors.impl;

import com.example.itaimarts.myapplication.domain.executor.Executor;
import com.example.itaimarts.myapplication.domain.executor.MainThread;
import com.example.itaimarts.myapplication.domain.interactors.GetServerPaymentApprovalInteractor;
import com.example.itaimarts.myapplication.domain.interactors.base.AbstractInteractor;
import com.example.itaimarts.myapplication.domain.network.PaymentApprovalRequest;


/**
 * Created by itai marts on 23/12/2016.
 */

public class GetServerPaymentApprovalInteractorImpl extends AbstractInteractor implements GetServerPaymentApprovalInteractor, PaymentApprovalRequest.Callback {

    private Callback presenter;
    private String paymentProvider;
    private String paymentApprovalToken;
    private PaymentApprovalRequest paymentApprovalRequestEndpoint;

    public GetServerPaymentApprovalInteractorImpl(Executor threadExecutor, MainThread mainThread,
                                                  String paymentProvider , String providerApprovalToken,
                                                  PaymentApprovalRequest paymentApprovalRequest,
                                                  Callback presenter) {
        super(threadExecutor, mainThread);
        this.paymentProvider = paymentProvider;
        this.paymentApprovalToken = providerApprovalToken;
        this.paymentApprovalRequestEndpoint = paymentApprovalRequest;
        this.presenter = presenter;
    }

    @Override
    public void run() {
        paymentApprovalRequestEndpoint.send(paymentApprovalToken,paymentProvider,this);
    }

    @Override
    public void paymentApproved() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                presenter.PaymentApprovedByServer();
            }
        });
    }

    @Override
    public void paymentNotApproved(final String message) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                presenter.PaymentDeniedByServer(message);
            }
        });
    }
}
