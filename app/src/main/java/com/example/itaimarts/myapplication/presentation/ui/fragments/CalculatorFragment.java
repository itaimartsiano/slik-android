package com.example.itaimarts.myapplication.presentation.ui.fragments;

import android.app.Fragment;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.itaimarts.myapplication.R;
import com.example.itaimarts.myapplication.storage.TableRepoImpl;


/**
 * Created by itai marts on 19/09/2016.
 */
public class CalculatorFragment extends Fragment implements View.OnClickListener {

    TextView subTotal;
    TextView totalSum;
    TextView currenyTotal;
    Button btn9;
    Button btn8;
    Button btn7;
    Button btn6;
    Button btn5;
    Button btn4;
    Button btn3;
    Button btn2;
    Button btn1;
    Button btn0;
    ImageButton bkspc;
    Button btn10Precent;
    Button btn12Precent;
    Button btn15Precent;
    Button btn0Precent;
    ImageButton btnX;
    TableRepoImpl tableRepo = TableRepoImpl.getInstance();
    //ImageButton btnV;/////////remove btn vee and listen in the activity to him!!

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v  = inflater.inflate(R.layout.fragement_calculator, container, false);
        subTotal = (TextView) v.findViewById(R.id.subtotal_payment_textView);
        totalSum = (TextView) v.findViewById(R.id.textView_total_sum);
        currenyTotal = (TextView) v.findViewById(R.id.currency_text_view_total1);
        btn9 = (Button) v.findViewById(R.id.btn_nine);
        btn8 = (Button) v.findViewById(R.id.btn_eight);
        btn7 = (Button) v.findViewById(R.id.btn_seven);
        btn6 = (Button) v.findViewById(R.id.btn_six);
        btn5 = (Button) v.findViewById(R.id.btn_five);
        btn4 = (Button) v.findViewById(R.id.btn_four);
        btn3 = (Button) v.findViewById(R.id.btn_three);
        btn2 = (Button) v.findViewById(R.id.btn_two);
        btn1 = (Button) v.findViewById(R.id.btn_one);
        btn0 = (Button) v.findViewById(R.id.btn_zero);
        btnX = (ImageButton) v.findViewById(R.id.btn_clear);
        //btnV = (ImageButton) v.findViewById(R.id.btn_vee);

        btn10Precent = (Button) v.findViewById(R.id.btn_ten_pre);
        btn12Precent = (Button) v.findViewById(R.id.btn_twelve_pre);
        btn15Precent = (Button) v.findViewById(R.id.btn_fifteen_pre);
        btn0Precent = (Button) v.findViewById(R.id.btn_zero_pre);
        bkspc = (ImageButton) v.findViewById(R.id.backspace_btn);

        totalSum.setAlpha(0);
        currenyTotal.setAlpha(0);
        subTotal.setText(String.valueOf(tableRepo.getTotalRequestSum()));

        btn9.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn1.setOnClickListener(this);
        btn0.setOnClickListener(this);
        //btnV.setOnClickListener(this);
        btnX.setOnClickListener(this);

        btn10Precent.setOnClickListener(this);
        btn12Precent.setOnClickListener(this);
        btn15Precent.setOnClickListener(this);
        btn0Precent.setOnClickListener(this);

        bkspc.setOnClickListener(this);

        return  v;
    }

    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_ten_pre:
                addPrecent(10);
                break;
            case R.id.btn_twelve_pre:
                addPrecent(12);
                break;
            case R.id.btn_fifteen_pre:
                addPrecent(15);
                break;
            case R.id.btn_zero_pre:
                addPrecent(0);
                break;
            case R.id.backspace_btn:
                backSpacePressed();
                break;
            case R.id.btn_clear:
                clearSubTotal();
                break;
            default:
                addNum(((Button)v).getText().toString());
                break;
        }
    }


    public void addNum(String num){
        if (subTotal.getText().length()<4){
            subTotal.setText(subTotal.getText() + num);
            totalSum.setAlpha(0);
            currenyTotal.setAlpha(0);
            tableRepo.setTipPrecentageRequest(0);
            tableRepo.setTotalRequestSum(Integer.parseInt(num));
        }
    }

    public void addPrecent(int num){
        if (subTotal.getText().length()>0){
            int subtotal = Integer.valueOf(subTotal.getText().toString());
            int totalInInt = subtotal* num / 100 +subtotal;
            totalSum.setText(String.valueOf(totalInInt));
            totalSum.setAlpha(1);
            currenyTotal.setAlpha(1);
            tableRepo.setTipPrecentageRequest(num);
            tableRepo.setTotalRequestSum(totalInInt);
        }
    }

    public void backSpacePressed(){
        String txt = String.valueOf(subTotal.getText());
        if (txt.length()>0){
            subTotal.setText(txt.substring(0,txt.length()-1));
            totalSum.setAlpha(0);
            currenyTotal.setAlpha(0);
            tableRepo.setTipPrecentageRequest(0);
        }
    }

    public void clearSubTotal(){
        totalSum.setText("");
        subTotal.setText("");
        currenyTotal.setAlpha(0);
        tableRepo.setTipPrecentageRequest(0);
    }


}
