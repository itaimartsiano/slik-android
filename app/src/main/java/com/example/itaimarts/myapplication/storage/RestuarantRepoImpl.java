package com.example.itaimarts.myapplication.storage;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by itai marts on 19/09/2016.
 */
public class RestuarantRepoImpl implements com.example.itaimarts.myapplication.domain.repository.RestuarantRepo {

    private static RestuarantRepoImpl repoInstance = null;
    private SharedPreferences sharedpreferences;

    private String restuarantID;
    private String name;
    private String info;
    private String address;
    private String smallPictureLink;
    private String bigPictureLink;

    private RestuarantRepoImpl(Context context){
        sharedpreferences = context.getSharedPreferences("Restuarant", Context.MODE_PRIVATE);
    }

    public static void init(Context context){
        if (repoInstance==null){
            repoInstance = new RestuarantRepoImpl(context);
        }
    }

    public static RestuarantRepoImpl getInstance(){
        return repoInstance;
    }

    public String getRestuarantID() {
        return restuarantID;
    }

    public void setRestuarantID(String restuarantID) {
        this.restuarantID = restuarantID;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getInfo() {
        return info;
    }

    @Override
    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String getSmallPictureLink() {
        return smallPictureLink;
    }

    @Override
    public void setSmallPictureLink(String smallPictureLink) {
        this.smallPictureLink = smallPictureLink;
    }

    @Override
    public String getBigPictureLink() {
        return bigPictureLink;
    }

    @Override
    public void setBigPictureLink(String bigPictureLink) {
        this.bigPictureLink = bigPictureLink;
    }
}
