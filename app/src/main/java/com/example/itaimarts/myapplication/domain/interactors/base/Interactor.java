package com.example.itaimarts.myapplication.domain.interactors.base;

/**
 * Created by itai marts on 14/08/2016.
 */
public interface Interactor {

    /**
     * This is the main method that starts an interactor. It will make sure that the interactor operation is done on a
     * background thread.
     */
    void execute();
}
