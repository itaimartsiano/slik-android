package com.example.itaimarts.myapplication.domain.interactors.impl;

import com.example.itaimarts.myapplication.domain.executor.Executor;
import com.example.itaimarts.myapplication.domain.executor.MainThread;
import com.example.itaimarts.myapplication.domain.interactors.GetPaymentURLInteractor;
import com.example.itaimarts.myapplication.domain.interactors.base.AbstractInteractor;
import com.example.itaimarts.myapplication.domain.model.Payment;
import com.example.itaimarts.myapplication.domain.network.PaymentURLRequest;
import com.example.itaimarts.myapplication.domain.repository.RestuarantRepo;
import com.example.itaimarts.myapplication.domain.repository.TableRepo;
import com.example.itaimarts.myapplication.domain.repository.UserDetailsRepo;
import com.example.itaimarts.myapplication.storage.RestuarantRepoImpl;
import com.example.itaimarts.myapplication.storage.TableRepoImpl;
import com.example.itaimarts.myapplication.storage.UserDetailsRepoImpl;

import retrofit.RetrofitError;

/**
 * Created by itai marts on 15/08/2016.
 */
public class GetPaymentURLInteractorImpl extends AbstractInteractor implements GetPaymentURLInteractor {

    private double paymentAmount;
    private PaymentURLRequest getUrlRequest;
    private callback callback;

    public GetPaymentURLInteractorImpl(Executor threadExecutor, MainThread mainThread, double paymentAmount, PaymentURLRequest getUrlRequest
    , callback orderDetailsPresenter) {
        super(threadExecutor, mainThread);
        this.paymentAmount = paymentAmount;
        this.getUrlRequest = getUrlRequest;
        this.callback = orderDetailsPresenter;
    }

    @Override
    public void run() {
        //TODO: remove the get instances from here
        UserDetailsRepo userDetailsRepo = UserDetailsRepoImpl.getInstance();
        TableRepo tableRepo = TableRepoImpl.getInstance();
        RestuarantRepo restuarantRepo = RestuarantRepoImpl.getInstance();
        Payment payment = null;
        String message = null;
        try {
            payment = getUrlRequest.sendRequest(userDetailsRepo.getUserSessionID(), tableRepo.getTableID(),
                    tableRepo.getVersion(), restuarantRepo.getRestuarantID(),
                    tableRepo.getTotalRequestSum(), "paypal", tableRepo.getCurrency()
                    , "Not implemented yet");
        }
        catch (RetrofitError e){
            //TODO: implement
            message = "got error from server";
        }



        if(payment != null){
            requestSucceed(payment);
        }else{
            requestFailed(message);
        }
    }


    public void requestSucceed(final Payment payment) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.paymentURLRequestSucceed(payment.url);
            }
        });
    }

    public void requestFailed(final String message) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.paymentURLRequestfailed(message);
            }
        });
    }
}
