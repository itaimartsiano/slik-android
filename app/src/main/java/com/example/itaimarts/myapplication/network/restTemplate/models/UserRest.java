package com.example.itaimarts.myapplication.network.restTemplate.models;

import java.util.Date;
import java.util.List;

import static android.R.id.list;

/**
 * Created by itai marts on 25/12/2016.
 */

public class UserRest {

    public Date birthday;
    public String picture; //URL
    public String email;
    public String name;
    public History history;
    public String session_id;



    public class History{
        List list;
        int lastUpdate;

    }
}
