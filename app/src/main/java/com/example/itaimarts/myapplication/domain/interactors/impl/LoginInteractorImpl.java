package com.example.itaimarts.myapplication.domain.interactors.impl;

import android.util.Log;

import com.example.itaimarts.myapplication.domain.executor.Executor;
import com.example.itaimarts.myapplication.domain.executor.MainThread;
import com.example.itaimarts.myapplication.domain.interactors.LoginInteractor;
import com.example.itaimarts.myapplication.domain.interactors.base.AbstractInteractor;
import com.example.itaimarts.myapplication.domain.model.User;
import com.example.itaimarts.myapplication.domain.network.LoginEndpoint;
import com.example.itaimarts.myapplication.domain.repository.UserDetailsRepo;

/**
 * Created by itai marts on 17/08/2016.
 */
public class LoginInteractorImpl extends AbstractInteractor implements LoginInteractor{

    private LoginEndpoint loginEndpoint;
    private LoginInteractor.callback activityPresenter;
    private String userId;
    private String facebookToken;
    private UserDetailsRepo userDetailsRepo;

    public LoginInteractorImpl(Executor threadExecutor, MainThread mainThread, LoginEndpoint login,
                               callback activityPresenter, String token, String userId, UserDetailsRepo userDetailsRepo) {
        super(threadExecutor, mainThread);
        this.loginEndpoint = login;
        this.activityPresenter = activityPresenter;
        this.facebookToken = token;
        this.userId = userId;
        this.userDetailsRepo = userDetailsRepo;
    }

    @Override
    public void run() {
        try {
            User user = loginEndpoint.send(facebookToken);
            onSuccessServerAns(user);
        }catch (Exception e){
            Log.e("",e.toString());
            onFailureServerAns("");
        }

    }



    public void onSuccessServerAns(User user) {

        updateRepo(user);

        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                activityPresenter.loginSuccess();
            }
        });

    }

    private void updateRepo(User user) {
        userDetailsRepo.setUserSessionID(user.sessionID);
        userDetailsRepo.setUserFacebookToken(facebookToken);
        userDetailsRepo.setEmail(user.email);
        userDetailsRepo.setUserName(user.name);
        userDetailsRepo.setImageUrl(user.imageUrl);
        userDetailsRepo.setBirthday(user.birthday);
    }


    public void onFailureServerAns(String message) {
        activityPresenter.loginFailed();
    }
}
