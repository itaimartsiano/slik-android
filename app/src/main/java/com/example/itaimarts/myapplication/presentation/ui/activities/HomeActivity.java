package com.example.itaimarts.myapplication.presentation.ui.activities;


import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.TabLayout;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.itaimarts.myapplication.R;
import com.example.itaimarts.myapplication.domain.executor.impl.ThreadExecutor;
import com.example.itaimarts.myapplication.domain.repository.UserDetailsRepo;
import com.example.itaimarts.myapplication.presentation.presenters.HomePresenter;
import com.example.itaimarts.myapplication.presentation.presenters.impl.HomePresenterImpl;
import com.example.itaimarts.myapplication.presentation.ui.fragments.HomeHistoryFragement;
import com.example.itaimarts.myapplication.presentation.ui.fragments.HomeMarketFragement;
import com.example.itaimarts.myapplication.storage.UserDetailsRepoImpl;
import com.example.itaimarts.myapplication.threading.MainThreadImpl;
import com.example.itaimarts.myapplication.utils.ImageUtils;
import com.example.itaimarts.myapplication.utils.Tweaks;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;


import java.util.ArrayList;
import java.util.List;


public class HomeActivity extends AppCompatActivity implements HomePresenter.View{

    private HomePresenter homePresenter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private UserDetailsRepo userDetailsRepo = UserDetailsRepoImpl.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        homePresenter = new HomePresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, getApplicationContext());

        updateUIWithUserProfile();
    }



    public void scanBarcode(View view){
        Log.d("HomeActivity","scan barcode called");
        Intent scanBarcodeActivity = new Intent(this, QRScannerActivity.class);
        scanBarcodeActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(scanBarcodeActivity);
    }



    public void configureSettings(View v){
        ///change to call configure setting to call HomePresenter
        Log.d("HomeActivity","configureSettings called");
        Intent settings = new Intent(this, SettingsActivity.class);
        settings.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(settings);
    }




    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new HomeHistoryFragement(), "History");
        adapter.addFragment(new HomeMarketFragement(), "Market");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void updateUIWithUserProfile() {
        TextView email = (TextView) findViewById(R.id.user_email_textView);
        TextView userName = (TextView) findViewById(R.id.user_profile_textView);
        ImageView profilePicture = (ImageView) findViewById(R.id.user_profile_picture);

        email.setText(userDetailsRepo.getEmail());
        userName.setText(userDetailsRepo.getUserName());
        Bitmap bitmap = userDetailsRepo.getProfilePicture();
        if (bitmap != null){
            profilePicture.setImageBitmap(bitmap);
        }
    }

    @Override
    public void updateHistoryTab() {

    }

    @Override
    public void updateMarketTab() {

    }

    @Override
    public void showInteractionScreen() {

    }



    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
