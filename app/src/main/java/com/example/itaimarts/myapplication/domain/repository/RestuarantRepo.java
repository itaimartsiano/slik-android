package com.example.itaimarts.myapplication.domain.repository;

/**
 * Created by itai marts on 20/09/2016.
 */
public interface RestuarantRepo {
    String getName();

    void setName(String name);

    String getInfo();

    void setInfo(String info);

    String getAddress();

    void setAddress(String address);

    String getSmallPictureLink();

    void setSmallPictureLink(String smallPictureLink);

    String getBigPictureLink();

    void setBigPictureLink(String bigPictureLink);

    public String getRestuarantID();

    public void setRestuarantID(String restuarantID);
}
