package com.example.itaimarts.myapplication.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;

import com.example.itaimarts.myapplication.domain.executor.Executor;
import com.example.itaimarts.myapplication.domain.executor.MainThread;
import com.example.itaimarts.myapplication.domain.interactors.LaunchSynchTableServiceInteractor;
import com.example.itaimarts.myapplication.domain.interactors.SaveTableDataFromServerInteractor;
import com.example.itaimarts.myapplication.domain.interactors.impl.LaunchSynchTableServiceInteractorImpl;
import com.example.itaimarts.myapplication.domain.interactors.impl.SaveTableDataFromServerimplInteractor;
import com.example.itaimarts.myapplication.domain.model.Product;
import com.example.itaimarts.myapplication.domain.model.Restaurant;
import com.example.itaimarts.myapplication.domain.model.Table;
import com.example.itaimarts.myapplication.domain.network.SynchTableService;
import com.example.itaimarts.myapplication.network.services.SynchTableServiceImpl;
import com.example.itaimarts.myapplication.presentation.presenters.AbstractPresenter;
import com.example.itaimarts.myapplication.presentation.presenters.OrderDetailsPresenter;
import com.example.itaimarts.myapplication.storage.ProductsRepoImpl;
import com.example.itaimarts.myapplication.storage.RestuarantRepoImpl;
import com.example.itaimarts.myapplication.storage.TableRepoImpl;
import com.example.itaimarts.myapplication.storage.UserDetailsRepoImpl;

import java.util.ArrayList;


/**
 * Created by itai marts on 22/08/2016.
 */

public class OrderDetailsPresenterImpl extends AbstractPresenter implements OrderDetailsPresenter, LaunchSynchTableServiceInteractor.callback,
        SaveTableDataFromServerInteractor.callback, SynchTableService.callback {

    private Context context;
    private View orderDetailsActivity;
    private SynchTableService synchTableService;

    public OrderDetailsPresenterImpl(Executor executor, MainThread mainThread,
                                     View orderDetailsActivity, Context context) {
        super(executor, mainThread);
        this.context = context;
        this.orderDetailsActivity = orderDetailsActivity;

    }


    @Override
    public void getTableInfo() {
        LaunchSynchTableServiceInteractor launchSynchTableServiceInteractor = new LaunchSynchTableServiceInteractorImpl(mExecutor,mMainThread,this, UserDetailsRepoImpl.getInstance(),
                ProductsRepoImpl.getInstance(), RestuarantRepoImpl.getInstance(), TableRepoImpl.getInstance(), new SynchTableServiceImpl(), this);
        launchSynchTableServiceInteractor.execute();
        synchTableService = launchSynchTableServiceInteractor.getSyncService();
        Log.d("OrderDetailsPresent","Launch Interactor called");
    }

    @Override
    public void payBtnPressed() {
        int sum = TableRepoImpl.getInstance().getTotalOrderSum();
        orderDetailsActivity.showCalcToUser();
    }

    @Override
    public void checkoutPayment(String provider) {
        orderDetailsActivity.showPaymentActivity(provider);
    }

    @Override
    public void finishPresenter() {
        if (synchTableService!= null){
            synchTableService.stopSynch();
        }
    }


    @Override
    public void serviceLaunchedSuccesfully() {

    }

    @Override
    public void serviceLaunchFailed() {

    }

    @Override
    public void newDataSaved() {
        Log.d("OrderDetailsPresenter","newDataSaved called");
        orderDetailsActivity.updateUiFromRepo();
    }

    @Override
    public void newDataFromServer(ArrayList<Product> products, Table table, Restaurant restaurant) {
        //TODO convert from preesentation model to Domain model
        SaveTableDataFromServerInteractor saveDataInteractor = new SaveTableDataFromServerimplInteractor(mExecutor,mMainThread,this,
                UserDetailsRepoImpl.getInstance(), ProductsRepoImpl.getInstance(),RestuarantRepoImpl.getInstance(), TableRepoImpl.getInstance(),
                products,restaurant,table);
        saveDataInteractor.execute();
        Log.d("OrderDetailsPresenter","newDataFromServer");
    }

    @Override
    public void synchFailed(String errMessage) {

    }







    /*
    Not implemented yet --------------------------------------------
     */
    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }
}
