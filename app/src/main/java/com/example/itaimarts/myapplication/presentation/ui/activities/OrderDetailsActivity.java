package com.example.itaimarts.myapplication.presentation.ui.activities;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.itaimarts.myapplication.R;
import com.example.itaimarts.myapplication.domain.executor.impl.ThreadExecutor;
import com.example.itaimarts.myapplication.presentation.presenters.OrderDetailsPresenter;
import com.example.itaimarts.myapplication.presentation.presenters.impl.OrderDetailsPresenterImpl;
import com.example.itaimarts.myapplication.presentation.ui.fragments.CalculatorFragment;
import com.example.itaimarts.myapplication.presentation.ui.fragments.ChoosePaymentMethodFragment;
import com.example.itaimarts.myapplication.presentation.ui.fragments.OrderDetailsMainTabFragement;
import com.example.itaimarts.myapplication.presentation.ui.fragments.OrderDetailsNoProductsFragement;
import com.example.itaimarts.myapplication.storage.ProductsRepoImpl;
import com.example.itaimarts.myapplication.storage.RestuarantRepoImpl;
import com.example.itaimarts.myapplication.threading.MainThreadImpl;

import java.net.URI;

import jp.wasabeef.blurry.Blurry;

public class OrderDetailsActivity extends FragmentActivity implements OrderDetailsPresenter.View{

    public static final String PAY_PAL = "PayPal";
    OrderDetailsPresenter orderDetailsPresenter;
    TextView restuarantName;
    ImageView restuarantBigImage;
    OrderDetailsMainTabFragement orderDetailsFragment;
    ProgressBar waitProgressBar;
    RelativeLayout orderDetailsMainTabLayout;
    OrderDetailsNoProductsFragement orderDetailsNoProductsFragement;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        init();
        orderDetailsPresenter.getTableInfo();
    }


    private void init(){
        orderDetailsPresenter = new OrderDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, getApplicationContext());
        restuarantBigImage = (ImageView) findViewById(R.id.restuarant_Big_imageView);
        restuarantName = (TextView) findViewById(R.id.resturant_name_TextView);
        waitProgressBar = (ProgressBar) findViewById(R.id.progressBarConnectionWithServer);
    }

    //pay button from order details screen pressed
    public void payButtonPressed(View view){
        orderDetailsPresenter.payBtnPressed();
    }

    //vee button from calculator pressed
    public void veePressed(View view){
        ChoosePaymentMethodFragment paymentMethodFragment = new ChoosePaymentMethodFragment();
        paymentMethodFragment.show(getSupportFragmentManager(),"payment_method");
    }

    //checkout with paypal pressed from ChoosePaymentMethod fragment
    public void checkoutPaymentWithPayPalPressed(){
        orderDetailsPresenter.checkoutPayment(PAY_PAL);
        orderDetailsPresenter.finishPresenter();
    }


    @Override
    public void updateUiFromRepo() {
        restuarantName.setText(RestuarantRepoImpl.getInstance().getName());
        //if there is products in table
        if (! ProductsRepoImpl.getInstance().getProductsAsArrayList().isEmpty()){
            if (orderDetailsFragment == null){
                orderDetailsFragment = new OrderDetailsMainTabFragement();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.add(R.id.fragment_container, orderDetailsFragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                //ft.addToBackStack("order");
                ft.commit();
            }else {
                //can make a Bug because the frgment transition is asynchronos
                orderDetailsFragment.updateUiFromRepo();
            }
        }else{
            if (orderDetailsNoProductsFragement == null){
                orderDetailsNoProductsFragement = new OrderDetailsNoProductsFragement();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.add(R.id.fragment_container, orderDetailsNoProductsFragement);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                //ft.addToBackStack("order");
                ft.commit();
            }
        }
        waitProgressBar.setAlpha(0);
    }


    @Override
    public void showCalcToUser() {

        // Create an instance of editorFrag
        CalculatorFragment calculatorFragment = new CalculatorFragment();

        //TODO: add method which disable and enable all the buttons on the screen - or just wrap the relevants with fragments - high priority~!!!

        orderDetailsMainTabLayout = (RelativeLayout) findViewById(R.id.order_details_main_screen_wrap);
        Blurry.with(getBaseContext()).radius(15).sampling(2).onto(orderDetailsMainTabLayout);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(R.id.fragment_container, calculatorFragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack("calc");
        ft.commit();
    }

    @Override
    public void showPaymentActivity(String provider) {
        Intent intent = new Intent(getBaseContext(),PaymentActivity.class);
        intent.putExtra("Provider", provider);
        startActivity(intent);
    }


    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount()>1){
            Blurry.delete(orderDetailsMainTabLayout);
            super.onBackPressed();
        }else {
            orderDetailsPresenter.finishPresenter();
            finish();
        }

    }


    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showError(String message) {

    }


}
