package com.example.itaimarts.myapplication.domain.model;

import com.example.itaimarts.myapplication.network.restTemplate.models.UserRest;

/**
 * Created by itai marts on 19/09/2016.
 */
public class Restaurant {

    public String buisnessID;
    public String name;
    public String info;
    public String address;
    public String smallPictureLink;
    public String bigPictureLink;

    public Restaurant(){
    }


    public Restaurant(String name, String info, String address, String smallPictureLink, String bigPictureLink) {
        this.name = name;
        this.info = info;
        this.address = address;
        this.smallPictureLink = smallPictureLink;
        this.bigPictureLink = bigPictureLink;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSmallPictureLink() {
        return smallPictureLink;
    }

    public void setSmallPictureLink(String smallPictureLink) {
        this.smallPictureLink = smallPictureLink;
    }

    public String getBigPictureLink() {
        return bigPictureLink;
    }

    public void setBigPictureLink(String bigPictureLink) {
        this.bigPictureLink = bigPictureLink;
    }
}
