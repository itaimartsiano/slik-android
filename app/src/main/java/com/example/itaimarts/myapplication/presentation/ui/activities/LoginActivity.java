package com.example.itaimarts.myapplication.presentation.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;

import com.example.itaimarts.myapplication.domain.executor.impl.ThreadExecutor;

import com.example.itaimarts.myapplication.R;
import com.example.itaimarts.myapplication.presentation.presenters.LoginPresenter;
import com.example.itaimarts.myapplication.presentation.presenters.impl.LoginPresenterImpl;
import com.example.itaimarts.myapplication.threading.MainThreadImpl;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.util.Arrays;

public class LoginActivity extends Activity implements LoginPresenter.View ,FacebookCallback<LoginResult> {

    CallbackManager callbackManager;
    LoginPresenter loginPresenter;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getBaseContext());
        setContentView(R.layout.activity_login);

        loginPresenter = new LoginPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, getApplicationContext());
        progressBar = (ProgressBar) findViewById(R.id.progressbarwaitingforserver);

        callbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);

        loginButton.setReadPermissions(Arrays.asList(
                "public_profile", "email", "user_birthday"));

        //set this class as the callback from facebook
        //TODO: In the next version need to replace with activity fragement which display validating with checkout server
        loginButton.registerCallback(callbackManager, this);
    }



    @Override
    public void showHomeActivity() {
        Log.d("LoginActivity","showHomeActivity called");
        Intent home = new Intent(getBaseContext(), HomeActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(home);
    }

    @Override
    public void showLoginActivity() {
        Log.d("LoginEndpoint Activity","showLoginActivity Called");
    }

    @Override
    public void showWaitingForCheckoutServerValidation() {
        //this is not working well
        progressBar.getIndeterminateDrawable().setAlpha(1);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("LoginActivity", "onActivityResult:"+data.getData());
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }




    //facebook methods
    @Override
    public void onSuccess(LoginResult loginResult) {
        Log.d(loginResult.getAccessToken().toString(),"");
        loginPresenter.UserApprovedAppInFacebook(loginResult.getAccessToken());
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onError(FacebookException error) {

    }
}
