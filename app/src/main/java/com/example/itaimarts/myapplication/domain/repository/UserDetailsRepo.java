package com.example.itaimarts.myapplication.domain.repository;

import android.graphics.Bitmap;

import java.util.Date;

/**
 * Created by itai marts on 15/08/2016.
 */
public interface UserDetailsRepo {
    public String getUserAppId();

    public void setUserAppId(String userAppId);

    public String getUserSessionID();

    public void setUserSessionID(String userSessionID);


    public String getUserFacebookToken();

    public void setUserFacebookToken(String userFacebookToken);


    public String getUserName();

    public void setUserName(String userName);


    public String getEmail();

    public void setEmail(String email);


    public void setProfilePicture(Bitmap bitmap);

    public Bitmap getProfilePicture();

    public String getImageUrl();

    public void setImageUrl(String imageUrl);

    public Date getBirthday();

    public void setBirthday(Date birthday);

    public void logOut();

}
