package com.example.itaimarts.myapplication.domain.interactors.impl;

import android.util.Log;

import com.example.itaimarts.myapplication.domain.executor.Executor;
import com.example.itaimarts.myapplication.domain.executor.MainThread;
import com.example.itaimarts.myapplication.domain.interactors.AlreadyLoggedInInteractor;
import com.example.itaimarts.myapplication.domain.interactors.base.AbstractInteractor;
import com.example.itaimarts.myapplication.domain.repository.UserDetailsRepo;

/**
 * Created by itai marts on 15/08/2016.
 */
public class AlreadyLoggedInInteractorImpl extends AbstractInteractor implements AlreadyLoggedInInteractor {

    private AlreadyLoggedInInteractor.callback mCallback;
    private UserDetailsRepo mUserDetailsRepo;


    public AlreadyLoggedInInteractorImpl(Executor threadExecutor, MainThread mainThread, callback callback, UserDetailsRepo userDetailsRepo) {
        super(threadExecutor, mainThread);
        mCallback = callback;
        mUserDetailsRepo = userDetailsRepo;
    }


    @Override
    public void run() {

        if (isUserAreadyLoggedIn()){
            Log.d("AlreadyLoggedInInterac","user was already logged in");
            mMainThread.post(new Runnable() {
                @Override
                public void run() {
                    mCallback.loggedIn();
                }
            });
        }else
        {
            Log.d("AlreadyLoggedInInterac","user was not logged in yet");
            mMainThread.post(new Runnable() {
                @Override
                public void run() {
                    mCallback.notLoggedIn();
                }
            });
        }
    }

    private boolean isUserAreadyLoggedIn(){
        String sessionID = mUserDetailsRepo.getUserSessionID();
        return (sessionID!=null);
    }

}
