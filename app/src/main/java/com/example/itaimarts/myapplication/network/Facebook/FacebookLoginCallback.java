package com.example.itaimarts.myapplication.network.Facebook;

import android.util.Log;

import com.example.itaimarts.myapplication.presentation.presenters.LoginPresenter;
import com.example.itaimarts.myapplication.storage.UserDetailsRepoImpl;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;

/**
 * Created by itai marts on 16/08/2016.
 */
public class FacebookLoginCallback implements FacebookCallback<LoginResult> {

    LoginPresenter loginPresenter;

    public FacebookLoginCallback(LoginPresenter loginPresenter){
        this.loginPresenter = loginPresenter;
    }

    @Override
    public void onSuccess(LoginResult loginResult) {

        //TODO: after success in entrance we should pass the data to presenter for next steps
        //fetch user data from facebook and save it in our storage
        new FetchFacebookUserData(UserDetailsRepoImpl.getInstance(),loginResult.getAccessToken().getUserId(),
                loginResult.getAccessToken()).execute();


        //update presenter user approved our app
        loginPresenter.UserApprovedAppInFacebook(loginResult.getAccessToken());
    }

    @Override
    public void onCancel() {
        Log.d("Facebook cancellation", "onCancel");
    }

    @Override
    public void onError(FacebookException error) {
        Log.e("Facebook app Exception", error.toString());

    }
}
