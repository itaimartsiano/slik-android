package com.example.itaimarts.myapplication.domain.repository;

/**
 * Created by itai marts on 20/09/2016.
 */
public interface TableRepo {
    String getTableID();

    void setTableID(String tableID);

    int getVersion();

    void setVersion(int version);

    int getTotalOrderSum();

    void setTotalOrderSum(int totalOrderSum);

    int getTotalRequestSum();

    void setTotalRequestSum(int totalRequestSum);

    String getCurrency();

    void setCurrency(String currency);
    int getTotalPayed();

    void setTotalPayed(int totalPayed);

}
