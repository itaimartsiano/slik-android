package com.example.itaimarts.myapplication.presentation.ui.fragments;

import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.itaimarts.myapplication.R;
import com.example.itaimarts.myapplication.domain.model.Product;
import com.example.itaimarts.myapplication.presentation.ui.adapters.ProductItemAdapter;
import com.example.itaimarts.myapplication.storage.ProductsRepoImpl;
import com.example.itaimarts.myapplication.storage.TableRepoImpl;

import java.util.ArrayList;


public class OrderDetailsMainTabFragement extends Fragment {

    View view = null;
    RelativeLayout mainRelativeLay;
    ProgressBar payementProgressBar;
    TextView payedTextView;
    TextView totalTextView;
    ImageView payedBubleImage;
    ListView productsListView;
    private final double PROGRESSBAR_WIDTH = 320;
    ArrayList<Product> arrayOfProducts;
    TableRepoImpl tableRepo = TableRepoImpl.getInstance();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_order_details_main_with_products, container, false);
        init();
        updateUiFromRepo();
        return view;
    }

    public void init(){
        mainRelativeLay = (RelativeLayout) view.findViewById(R.id.order_details_main_screen);
        payementProgressBar = (ProgressBar) view.findViewById(R.id.progressBar_payement);
        payedTextView = (TextView) view.findViewById(R.id.total_paid_TextView);
        totalTextView = (TextView) view.findViewById(R.id.total_amount_TextView);
        payedBubleImage = (ImageView) view.findViewById(R.id.tooltip_bubble_imageView);
        productsListView = (ListView) view.findViewById(R.id.products_listView);
    }


    public void updateUiFromRepo() {
        arrayOfProducts = ProductsRepoImpl.getInstance().getProductsAsArrayList();
        updateProgressBarWithPrice();
        updateProductsListView(arrayOfProducts);
        mainRelativeLay.setAlpha(1);
    }



    //update progress bar with products user choose to pay
    public void updateProgressBarWithPrice(){
        String currency = tableRepo.getCurrency();
        double totalPrice = tableRepo.getTotalOrderSum();
        double totalRequestSum =0;

        for (Product product:arrayOfProducts){
            totalRequestSum += product.getSelected()* product.getPrice();
        }
        if (totalRequestSum>0){
            //    sum = Math.round(sum*100);
            //    sum = sum/100;
            totalRequestSum = Math.round(totalRequestSum);
        }
        tableRepo.setTotalRequestSum((int) totalRequestSum);

        totalTextView.setText(totalPrice + " " + currency);
        payedTextView.setText(totalRequestSum + " " + currency);
        payementProgressBar.setMax((int) totalPrice);
        //payementProgressBar.setProgress((int)sum);
        setProgressAnimate(payementProgressBar, (int) totalRequestSum);
        int marginFromLeft = -6;
        if(totalPrice>0){
            marginFromLeft = (int) ((PROGRESSBAR_WIDTH / totalPrice * totalRequestSum) - 8);
        }

        int marginFromLeftPX = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, marginFromLeft, getResources().getDisplayMetrics());
        int marginFromTop = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics());

        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) payedBubleImage.getLayoutParams();
        lp.setMargins(marginFromLeftPX,marginFromTop,0,0);
        payedBubleImage.setLayoutParams(lp);
    }



    //update the progress bar from server pay data
    private void updateProgressBarWithPriceAlreadyPayed(int totalPrice, int payed, String currency){
        totalTextView.setText(totalPrice + " " + currency);
        payedTextView.setText(payed + " " + currency);
        payementProgressBar.setMax(totalPrice);
        payementProgressBar.setProgress(payed);

        int marginFromLeft = -6;
        if(totalPrice>0){
            marginFromLeft = (int) ((PROGRESSBAR_WIDTH / totalPrice * payed) - 8);
        }

        int marginFromLeftPX = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, marginFromLeft, getResources().getDisplayMetrics());
        int marginFromTop = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics());

        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) payedBubleImage.getLayoutParams();
        lp.setMargins(marginFromLeftPX,marginFromTop,0,0);
        payedBubleImage.setLayoutParams(lp);

    }


    private void setProgressAnimate(ProgressBar pb, int progressTo)
    {
        ObjectAnimator animation = ObjectAnimator.ofInt(pb, "progress", pb.getProgress(), progressTo);
        animation.setDuration(300);
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();
    }


    private void updateProductsListView(ArrayList<Product> products){
        // Construct the data source
        arrayOfProducts = products;
        // Create the adapter to convert the array to views
        ProductItemAdapter adapter = new ProductItemAdapter(getActivity(), arrayOfProducts,this);
        // Attach the adapter to a ListView
        productsListView.setAdapter(adapter);
        productsListView.deferNotifyDataSetChanged();
    }


}
