package com.example.itaimarts.myapplication.domain.interactors;

import com.example.itaimarts.myapplication.domain.interactors.base.Interactor;

/**
 * Created by itai marts on 15/08/2016.
 */
public interface SaveTableDataFromServerInteractor extends Interactor{

    interface callback{
        void newDataSaved();
    }

}
