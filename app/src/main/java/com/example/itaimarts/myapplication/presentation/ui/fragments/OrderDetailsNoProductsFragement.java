package com.example.itaimarts.myapplication.presentation.ui.fragments;

import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.itaimarts.myapplication.R;
import com.example.itaimarts.myapplication.domain.model.Product;
import com.example.itaimarts.myapplication.presentation.ui.adapters.ProductItemAdapter;
import com.example.itaimarts.myapplication.storage.ProductsRepoImpl;
import com.example.itaimarts.myapplication.storage.TableRepoImpl;

import java.util.ArrayList;


public class OrderDetailsNoProductsFragement extends Fragment {

    View view = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_order_details_main_no_products, container, false);
        return view;
    }




}
