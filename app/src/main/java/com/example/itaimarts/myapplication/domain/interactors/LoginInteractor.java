package com.example.itaimarts.myapplication.domain.interactors;

import com.example.itaimarts.myapplication.domain.interactors.base.Interactor;
import com.example.itaimarts.myapplication.domain.model.User;

import java.util.Iterator;

/**
 * Created by itai marts on 17/08/2016.
 */
public interface LoginInteractor extends Interactor{

    interface callback{
        void loginSuccess();
        void loginFailed();
    }

}
