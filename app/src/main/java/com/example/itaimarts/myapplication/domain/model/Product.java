package com.example.itaimarts.myapplication.domain.model;

/**
 * Created by itai marts on 18/09/2016.
 */
public class Product {
    private String productName;
    private double price;
    private int amount;
    private int selected;
    private String ID;

    public Product(String productName, double price, int amount, int selected, String ID) {
        this.productName = productName;
        this.ID = ID;
        this.price = price;
        this.amount = amount;
        this.selected = selected;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getID() {
        return ID;
    }

    public void setID(String  ID) {
        this.ID = ID;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }
}
