package com.example.itaimarts.myapplication.presentation.presenters.impl;

import android.content.Context;

import com.example.itaimarts.myapplication.domain.executor.Executor;
import com.example.itaimarts.myapplication.domain.executor.MainThread;
import com.example.itaimarts.myapplication.domain.interactors.GetUserDInteractor;
import com.example.itaimarts.myapplication.domain.interactors.impl.GetUserDInteractorImpl;
import com.example.itaimarts.myapplication.domain.network.GetUserDetailsEndPoint;
import com.example.itaimarts.myapplication.network.checkoutServer.GetMe;
import com.example.itaimarts.myapplication.presentation.presenters.AbstractPresenter;
import com.example.itaimarts.myapplication.presentation.presenters.HomePresenter;
import com.example.itaimarts.myapplication.storage.UserDetailsRepoImpl;


/**
 * Created by itai marts on 22/08/2016.
 */
public class HomePresenterImpl extends AbstractPresenter implements HomePresenter, GetUserDInteractor.callback {

    private Context context;
    private HomePresenter.View homePresenterActivity;
    private GetUserDInteractor getUserDInteractor;

    public HomePresenterImpl(Executor executor, MainThread mainThread,
                             HomePresenter.View homePresenterActivity, Context context) {
        super(executor, mainThread);
        this.context = context;
        this.homePresenterActivity = homePresenterActivity;
        GetUserDetailsEndPoint getUserDetailsEndPoint = new GetMe();
        getUserDInteractor = new GetUserDInteractorImpl(executor,mainThread,getUserDetailsEndPoint
                                                        ,this, UserDetailsRepoImpl.getInstance());
        getUserDInteractor.execute();
    }


    @Override
    public void scanBtnPressed() {
        //TODO: implement
        //launch synch service
    }

    @Override
    public void newDataSaved() {
        homePresenterActivity.updateUIWithUserProfile();
    }

    @Override
    public void requestFailed() {

    }
}
