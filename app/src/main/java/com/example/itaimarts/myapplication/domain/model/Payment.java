package com.example.itaimarts.myapplication.domain.model;

import java.security.PublicKey;

/**
 * Created by itai marts on 11/01/2017.
 */

public class Payment {

    public String vendor;
    public String currency;
    public int subTotal;
    public int total;
    public String description;
    public String transcationId;
    public String url;
    public PaymentStatus paymentStatus;


    public enum PaymentStatus{
        INIT_REQUEST, WAITING_FOR_URL, WAITING_FOR_USER_PAYMENT,
        WAITING_FOR_VALIDATION, PAYMENT_APPROVED
    }

}

