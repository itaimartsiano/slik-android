package com.example.itaimarts.myapplication.domain.model;

import java.util.ArrayList;

/**
 * Created by itai marts on 19/09/2016.
 */
public class Table {

    public String tableID;
    public Restaurant restaurant;
    public Bill bill;

    public Table(){
        this.bill = new Bill();
        this.restaurant = new Restaurant();
    }

    public Table(ArrayList<Product> arrayList, Restaurant restaurant, int totalSum, int totalPayed, String currency, int version,
                 String tableID) {
        this.bill = new Bill();
        this.restaurant = new Restaurant();
        bill.products = arrayList;
        this.restaurant = restaurant;
        bill.totalSum = totalSum;
        bill.totalPayed = totalPayed;
        bill.currency = currency;
        bill.version = version;
        this.tableID = tableID;
    }

    public String getTableID() {
        return tableID;
    }

    public void setTableID(String tableID) {
        this.tableID = tableID;
    }

    public ArrayList<Product> getArrayList() {
        return bill.products;
    }

    public void setArrayList(ArrayList<Product> arrayList) {
        bill.products = arrayList;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public int getTotalSum() {
        return bill.totalSum;
    }

    public void setTotalSum(int totalSum) {
        bill.totalSum = totalSum;
    }

    public int getTotalPayed() {
        return bill.totalPayed;
    }

    public void setTotalPayed(int totalPayed) {
        bill.totalPayed = totalPayed;
    }

    public String getCurrency() {
        return bill.currency;
    }

    public void setCurrency(String currency) {
        bill.currency = currency;
    }

    public int getVersion() {
        return bill.version;
    }

    public void setVersion(int version) {
        bill.version = version;
    }
}
