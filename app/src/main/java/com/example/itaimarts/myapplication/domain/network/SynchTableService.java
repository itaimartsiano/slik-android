package com.example.itaimarts.myapplication.domain.network;

import com.example.itaimarts.myapplication.domain.model.Product;
import com.example.itaimarts.myapplication.domain.model.Restaurant;
import com.example.itaimarts.myapplication.domain.model.Table;
import com.example.itaimarts.myapplication.domain.repository.TableRepo;
import com.example.itaimarts.myapplication.domain.repository.UserDetailsRepo;


import java.util.ArrayList;

/**
 * Created by itai marts on 17/08/2016.
 */
public interface SynchTableService {

    public void startSynch();
    public void stopSynch();
    public void initService(callback callback, UserDetailsRepo userDetailsRepo, TableRepo tableRepo);

    public interface callback{
        public void newDataFromServer(ArrayList<Product> products, Table table, Restaurant restaurant);
        public void synchFailed(String errMessage);
    }


}
