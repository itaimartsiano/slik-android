package com.example.itaimarts.myapplication.network.checkoutServer.old_config;


import com.example.itaimarts.myapplication.domain.network.PaymentApprovalRequest;

/**
 * Created by itai marts on 24/12/2016.
 */

public class PaymentApprovalRequestImpl implements PaymentApprovalRequest {


    @Override
    public void send(String approvalToken, String paymentProvider, Callback interactor) {
        //TODO: implement
        interactor.paymentApproved();
    }
}
