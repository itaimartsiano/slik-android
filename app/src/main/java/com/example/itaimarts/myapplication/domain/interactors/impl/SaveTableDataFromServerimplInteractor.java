package com.example.itaimarts.myapplication.domain.interactors.impl;

import android.util.Log;

import com.example.itaimarts.myapplication.domain.executor.Executor;
import com.example.itaimarts.myapplication.domain.executor.MainThread;
import com.example.itaimarts.myapplication.domain.interactors.SaveTableDataFromServerInteractor;
import com.example.itaimarts.myapplication.domain.interactors.base.AbstractInteractor;
import com.example.itaimarts.myapplication.domain.model.Product;
import com.example.itaimarts.myapplication.domain.model.Restaurant;
import com.example.itaimarts.myapplication.domain.model.Table;
import com.example.itaimarts.myapplication.domain.repository.ProductsRepo;
import com.example.itaimarts.myapplication.domain.repository.RestuarantRepo;
import com.example.itaimarts.myapplication.domain.repository.TableRepo;
import com.example.itaimarts.myapplication.domain.repository.UserDetailsRepo;

import java.util.ArrayList;

/**
 * Created by itai marts on 15/08/2016.
 */
public class SaveTableDataFromServerimplInteractor extends AbstractInteractor implements SaveTableDataFromServerInteractor {

    private callback mCallback;
    private UserDetailsRepo mUserDetailsRepo;
    private ProductsRepo mProductsRepo;
    private RestuarantRepo mRestuarantRepo;
    private TableRepo mTableRepo;
    private ArrayList<Product> products;
    private Restaurant restaurant;
    private Table table;

    public SaveTableDataFromServerimplInteractor(Executor threadExecutor, MainThread mainThread, callback mCallback, UserDetailsRepo mUserDetailsRepo,
                                                 ProductsRepo mProductsRepo, RestuarantRepo mRestuarantRepo, TableRepo mTableRepo,
                                                 ArrayList<Product> products, Restaurant restaurant, Table table) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mUserDetailsRepo = mUserDetailsRepo;
        this.mProductsRepo = mProductsRepo;
        this.mRestuarantRepo = mRestuarantRepo;
        this.mTableRepo = mTableRepo;
        this.products = products;
        this.restaurant = restaurant;
        this.table = table;
    }

    @Override
    public void run() {
        Log.d("SaveTableDataIntera","started");

        //check if its the first time we get data
        if (mRestuarantRepo.getAddress() == null){
            mRestuarantRepo.setAddress(restaurant.getAddress());
            mRestuarantRepo.setBigPictureLink(restaurant.getBigPictureLink());
            mRestuarantRepo.setInfo(restaurant.getInfo());
            mRestuarantRepo.setName(restaurant.getName());
            mRestuarantRepo.setSmallPictureLink(restaurant.getSmallPictureLink());
            mRestuarantRepo.setRestuarantID(restaurant.buisnessID);
        }

        if (table.getCurrency() != null){
            mTableRepo.setCurrency(table.getCurrency());
        }

        mTableRepo.setTableID(table.getTableID());
        mTableRepo.setTotalPayed(table.getTotalPayed());
        mTableRepo.setTotalOrderSum(table.getTotalSum());
        mTableRepo.setVersion(table.getVersion());

        updateNewDataFromOld(products);
        mProductsRepo.setProductsAsArrayList(products);

        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.newDataSaved();
            }
        });
    }


    private void updateNewDataFromOld(ArrayList<Product> products){
        ArrayList<Product> oldProducts = mProductsRepo.getProductsAsArrayList();
        if (oldProducts!=null && products!=null){
            for (Product newProduct: products){
                for (Product oldProduct: oldProducts){
                    if (newProduct.getID().equals(oldProduct.getID())){
                        newProduct.setSelected(oldProduct.getSelected());
                    }
                }
            }
        }
    }


}
