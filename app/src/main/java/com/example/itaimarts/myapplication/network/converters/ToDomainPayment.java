package com.example.itaimarts.myapplication.network.converters;

import com.example.itaimarts.myapplication.domain.model.Payment;
import com.example.itaimarts.myapplication.network.checkoutServer.GetPaymentStatus;
import com.example.itaimarts.myapplication.network.restTemplate.models.InitPaymentResponseRest;

/**
 * Created by itai marts on 17/01/2017.
 */

public class ToDomainPayment {

    public static Payment convert(GetPaymentStatus.PaymentStatusResponse response, String transactionId) {
        Payment payment = new Payment();
        payment.transcationId = transactionId;
        if ("COMPLETE".equals(payment.paymentStatus)){
            payment.paymentStatus = Payment.PaymentStatus.PAYMENT_APPROVED;
            return payment;
        }
        payment.paymentStatus = Payment.PaymentStatus.WAITING_FOR_VALIDATION;
        return payment;
    }

    public static Payment convert(InitPaymentResponseRest serverResponse){
        Payment paymentStatus = new Payment();
        paymentStatus.transcationId = serverResponse.transactionId;
        paymentStatus.url = serverResponse.url;
        return paymentStatus;
    }
}
