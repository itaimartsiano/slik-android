package com.example.itaimarts.myapplication.presentation.ui.fragments;





import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.itaimarts.myapplication.R;
import com.example.itaimarts.myapplication.presentation.ui.activities.OrderDetailsActivity;
import com.example.itaimarts.myapplication.storage.TableRepoImpl;

/**
 * Created by itai marts on 17/12/2016.
 */

public class ChoosePaymentMethodFragment extends DialogFragment {

    Button btnCancel;
    TextView outOfWhich;
    TextView totalSum;
    OrderDetailsActivity orderDetailsActivity;
    TableRepoImpl tableRepo = TableRepoImpl.getInstance();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragement_payment_method, container);
        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        orderDetailsActivity = (OrderDetailsActivity) getActivity();

        RelativeLayout checkoutLayout = (RelativeLayout) view.findViewById(R.id.lower_layout);
        checkoutLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderDetailsActivity.checkoutPaymentWithPayPalPressed();
                getDialog().dismiss();
            }
        });

        btnCancel = (Button)view.findViewById(R.id.cancel_payment_dialog);
        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        outOfWhich = (TextView) view.findViewById(R.id.out_of_which);
        totalSum = (TextView) view.findViewById(R.id.toatal_sum_text);

        int partOfSum = (tableRepo.getTotalRequestSum() * tableRepo.getTipPrecentageRequest()) / 100;
        String sentnce = String.format("out of which %d %s (%d %%) is tip.", partOfSum,tableRepo.getCurrency(),tableRepo.getTipPrecentageRequest() );
        outOfWhich.setText(sentnce);

        String sumWithCurrency = tableRepo.getTotalRequestSum()+ " "+tableRepo.getCurrency();
        totalSum.setText(sumWithCurrency);

        return view;
    }




}
