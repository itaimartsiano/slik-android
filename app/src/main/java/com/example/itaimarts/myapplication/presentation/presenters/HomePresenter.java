package com.example.itaimarts.myapplication.presentation.presenters;

import android.graphics.Bitmap;

import com.example.itaimarts.myapplication.presentation.ui.BaseView;

/**
 * Created by itai marts on 22/08/2016.
 */
public interface HomePresenter {

    public void scanBtnPressed();


    interface View extends BaseView{
        public void updateUIWithUserProfile();
        public void updateHistoryTab();
        public void updateMarketTab();
        public void showInteractionScreen();

    }

}
