package com.example.itaimarts.myapplication.network.restTemplate.models;


import java.util.Date;
import java.util.List;

/**
 * Created by itai marts on 04/01/2017.
 */

public class GroupInfoDetailsRest {

    public String business;
    public int bill_index;
    public String identifier;
    public String name;
    public Bill bill;

    public class Bill{
        public String id;
        public List <UserAction> actions;
        public List <Items> items;
        public int paid_value;
        public int total_value;


        public class UserAction {
            public String who;
            public String what;
            public Date when;
            public String _id;
        }

        public class Items {
            public String name;
            public int price;
            public int quantity;
            public String _id;
        }

    }




}
