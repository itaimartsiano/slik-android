package com.example.itaimarts.myapplication.presentation.ui.activities;

import android.app.Activity;

import android.app.VoiceInteractor;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;


import com.example.itaimarts.myapplication.R;
import com.example.itaimarts.myapplication.domain.executor.impl.ThreadExecutor;
import com.example.itaimarts.myapplication.domain.repository.TableRepo;
import com.example.itaimarts.myapplication.network.checkoutServer.EndPointBase;
import com.example.itaimarts.myapplication.presentation.presenters.PaymentPresenter;
import com.example.itaimarts.myapplication.presentation.presenters.impl.PaymentPresenterImpl;
import com.example.itaimarts.myapplication.storage.TableRepoImpl;
import com.example.itaimarts.myapplication.threading.MainThreadImpl;
import com.paypal.android.sdk.payments.PayPalPayment;

import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;

/**
 * Created by itai marts on 19/09/2016.
 */
public class PaymentActivity extends Activity implements PaymentPresenter.View {

    WebView webView;
    String paymentProvider;
    PaymentPresenter paymentPresenter;
    TableRepo tableRepo = TableRepoImpl.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //TODO: add inizializing payment or progress circle
        init();

    }

    private void init(){
        paymentProvider = getIntent().getStringExtra("Provider");
        double amount = tableRepo.getTotalRequestSum();
        paymentPresenter = new PaymentPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(),this,getApplicationContext());
        paymentPresenter.getPaymentURL(paymentProvider,amount);
    }

    @Override
    public void showUserCheckoutPaymentScreen(String PaymentURL) {
        webView = new WebView(this);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient(){
            //this method listen to communocation with the provider server
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, final String url) {
                String [] array = new String[]{url};
                array[0] = array[0].replace("localhost", "192.168.1.4");
                new getURLPage().execute(array);
                return true;
            }
        });
        setContentView(webView);
        webView.loadUrl(PaymentURL);
    }


    @Override
    public void showUserPaymentSucceedScreen() {
        Intent intent = new Intent(getBaseContext(),OrderDetailsActivity.class);
        intent.setFlags(FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    //when user make change in the page, the webview client will make URLPaymentApprovalHandler(getURLPage)
    //which decide which page to show user - in case the message Payment Approved recieved,



    //this inner class use to "listen" communication between user and provider
    private class getURLPage extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
        }


        @Override
        protected String doInBackground(String... params) {
            try {
                //TODO: check not null
                String url = params[0];
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
                String paymentUrl = restTemplate.getForObject(url,String.class);
                return paymentUrl;

            } catch (Exception e) {
                Log.e("PaymentActivity",e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s!=null && s.contains("payment accepted")){
                //dont need to show the page, just show progressbar and trigger the payment against server flow.
                //webView.loadData(s, "text/html", "UTF-8");
                //TODO:method which find paypal approvment token
                paymentPresenter.userPaymentSucceed("providerApprovmentToken");

            }else{
                webView.loadUrl("http://www.nana.com");
            }

        }
    }


    /*
    Not implemented yet -----------------------------------------
     */
    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showError(String message) {

    }


}
