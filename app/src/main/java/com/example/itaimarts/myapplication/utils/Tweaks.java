package com.example.itaimarts.myapplication.utils;

/**
 * Created by itai marts on 29/09/2016.
 */

public class Tweaks {

    public static final Boolean CAMERA_ENABLED = false;
    public static final boolean SERVER_ENABLED = false;
    public static final String TABLE_SAMPLE_ID = "GROUPIDENT";
    public static final String SESSION_ID_SAMPLE = "this_is_session_idS";

}
