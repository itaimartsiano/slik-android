package com.example.itaimarts.myapplication.network.converters;

import com.example.itaimarts.myapplication.domain.model.Product;
import com.example.itaimarts.myapplication.domain.model.Table;
import com.example.itaimarts.myapplication.network.restTemplate.models.GroupInfoDetailsRest;

import java.util.ArrayList;

/**
 * Created by itai marts on 17/01/2017.
 */

public class ToDomainTable {

    public static Table convert(GroupInfoDetailsRest groupInfoDetails){
        Table table = new Table();
        table.restaurant.buisnessID = groupInfoDetails.business;
        table.bill.version = groupInfoDetails.bill_index;
        table.bill.totalSum = groupInfoDetails.bill.total_value;
        table.bill.totalPayed = groupInfoDetails.bill.paid_value;
        table.tableID = groupInfoDetails.identifier;
        ArrayList<Product> products = new ArrayList<>();
        for (GroupInfoDetailsRest.Bill.Items item : groupInfoDetails.bill.items){
            Product product = new Product(item.name,item.price,item.quantity,0,item._id);
            products.add(product);
        }
        table.bill.products = products;
        return table;
    }

}
