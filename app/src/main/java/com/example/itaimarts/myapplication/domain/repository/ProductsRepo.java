package com.example.itaimarts.myapplication.domain.repository;

import com.example.itaimarts.myapplication.domain.model.Product;

import java.util.ArrayList;

/**
 * Created by itai marts on 20/09/2016.
 */
public interface ProductsRepo {
    void insertProduct(com.example.itaimarts.myapplication.domain.model.Product product);

    void deleteProduct(com.example.itaimarts.myapplication.domain.model.Product product);

    ArrayList<Product> getProductsAsArrayList();

    void setProductsAsArrayList(ArrayList<Product> products);

    void clearData();

    void replaceProduct(Product oldProduct,
                        Product newProduct);
}
