package com.example.itaimarts.myapplication.presentation.presenters;

import com.example.itaimarts.myapplication.presentation.ui.BaseView;

/**
 * Created by itai marts on 15/08/2016.
 */
public interface SplashPresenter extends BasePresenter {

    interface View extends BaseView{
        void showHomeActivity();
        void showLoginActivity();
    }

    void checkIfAlreadyLoggedIn();

}
