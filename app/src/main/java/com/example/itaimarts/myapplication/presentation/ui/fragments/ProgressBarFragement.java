package com.example.itaimarts.myapplication.presentation.ui.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.itaimarts.myapplication.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProgressBarFragement extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.progress_bar_fragement, container, false);
    }

}
