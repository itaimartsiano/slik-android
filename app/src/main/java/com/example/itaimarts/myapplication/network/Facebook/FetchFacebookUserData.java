package com.example.itaimarts.myapplication.network.Facebook;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;


import com.example.itaimarts.myapplication.domain.repository.UserDetailsRepo;


import com.example.itaimarts.myapplication.utils.ImageUtils;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;


import org.json.JSONException;
import org.json.JSONObject;



/**
 * Created by itai marts on 22/08/2016.
 */
public class FetchFacebookUserData extends AsyncTask {

    private UserDetailsRepo mUserDetailsRepo;
    private String facebookUserId;
    private AccessToken facebookAccessToken;

    public FetchFacebookUserData(UserDetailsRepo userDetailsRepo, String facebookUserId, AccessToken facebookAccessToken){
        mUserDetailsRepo = userDetailsRepo;
        this.facebookUserId = facebookUserId;
        this.facebookAccessToken = facebookAccessToken;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        fetchAndSave();
        return null;
    }


    public void fetchAndSave() {

        //save user details in memory
        //mUserDetailsRepo.setUserFacebookId(facebookUserId);
        mUserDetailsRepo.setUserFacebookToken(facebookAccessToken.getToken());


        //fetch profile picture from facebook
        Bitmap profilePicture = ImageUtils.DownloadImageFromURL("https://graph.facebook.com/"+ facebookUserId +"/picture?type=large");
        mUserDetailsRepo.setProfilePicture(profilePicture);


        ProfileTracker profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                this.stopTracking();
                Profile.setCurrentProfile(currentProfile);
                mUserDetailsRepo.setUserName(currentProfile.getName());
            }
        };
        profileTracker.startTracking();



    }


}
