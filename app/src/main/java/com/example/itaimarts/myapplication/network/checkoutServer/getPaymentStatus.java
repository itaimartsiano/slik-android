package com.example.itaimarts.myapplication.network.checkoutServer;

import com.example.itaimarts.myapplication.domain.model.Payment;
import com.example.itaimarts.myapplication.domain.model.User;
import com.example.itaimarts.myapplication.domain.network.PaymentApprovalRequest;
import com.example.itaimarts.myapplication.network.converters.ToDomainPayment;
import com.example.itaimarts.myapplication.network.restTemplate.models.GroupInfoDetailsRest;
import com.example.itaimarts.myapplication.network.restTemplate.models.UserRest;

import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Path;

/**
 * Created by itai marts on 24/12/2016.
 */


//TODO: need to fix - not working
public class GetPaymentStatus extends EndPointBase {

    protected endPointAPI endPointAPI;

    public GetPaymentStatus(){
        restAdapter = new RestAdapter.Builder().setEndpoint(checkoutServer).build();
        endPointAPI = restAdapter.create(endPointAPI.class);
    }


    public Payment sendRequest(String sessionID, String transactionId){
        PaymentStatusResponse paymentStatusResponse = endPointAPI.getPaymentStatus(sessionID, transactionId);
        return ToDomainPayment.convert(paymentStatusResponse, transactionId);
    }


    ////////////////////////////////////////////////////////////////
    private interface endPointAPI {

        @GET("/payments/{transactionId}/status")
        PaymentStatusResponse getPaymentStatus(@Header("x-session-id") String sessionID,
                                               @Path("transactionId") String transactionId);

    }


    public class PaymentStatusResponse{
        public String status;
    }


}

