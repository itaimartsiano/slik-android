package com.example.itaimarts.myapplication.domain.executor;

import com.example.itaimarts.myapplication.domain.interactors.base.AbstractInteractor;

/**
 * Created by itai marts on 14/08/2016.
 */
public interface Executor {

    /**
     * This method should call the interactor's run method and thus start the interactor. This should be called
     * on a background thread as interactors might do lengthy operations.
     *
     * @param interactor The interactor to run.
     */
    void execute(final AbstractInteractor interactor);
}
